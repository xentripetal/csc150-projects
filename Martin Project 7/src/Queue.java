/**
 * Creates and manages a queue ADT
 *@author Corbin Martin
 * Invariants
 * Size is always >= 0
 * If size is 1, firstNode always equals lastNode
 * firstNode and lastNode are linked via there nodes
 * If is empty, firstNode and lastNode are null.
 */
public class Queue<T> {
	QueueNode<T> firstNode;
	QueueNode<T> lastNode;
	int size;

	/**
	 * Constructs a Queue with nothing inside
	 */
	public Queue() {
		size = 0;
		lastNode = null;
		firstNode = null;
	}

	/**
	 *@return true if empty, false if not
	 */
	public boolean isEmpty() {
		return (firstNode == null);
	}

	/**
	 * Lets users see last inserted element. 
	 * returns null if Queue is empty
	 * @return Last inserted element
	 */
	public T peekLast() {
		if (isEmpty())
			return null;
		else
			return lastNode.value;
	}


	/**
	 * @return True if Queue contains matching element
	 */
	public boolean contains(T check) {
		QueueNode<T> runner = firstNode;
		boolean isFound = false;
		while (runner != null && !isFound) {
			isFound = runner.value.equals(check);
			runner = runner.next;
		}
		return isFound;
	}

	/**
	 * Adds value to the queue.
	 *
	 * @param value
	 *            object to add
	 */
	public void enqueue(T value) {
		QueueNode<T> newNode = new QueueNode<T>(value);
		if (isEmpty())
			firstNode = lastNode = newNode;
		else {
			lastNode.next = newNode;
			lastNode = newNode;
		}
		size++;
	}

	/**
	 * Removes object at beginning of queue
	 * @param Object that was removed
	 */
	public T dequeue() {
		T toReturn = null;
		if (isEmpty())
			toReturn = null;
		else if (size() == 1) {
			toReturn = firstNode.value;
			firstNode = lastNode = null;
			size--;
		}
		else {
			toReturn = firstNode.value;
			firstNode = firstNode.next;
			size--;
		}
		return toReturn;
	}

	/**
	 * @return Number of elements in Queue
	 */
	public int size() {
		return size;
	}

	/**
	 * Creates a clone of the queue.
	 * Non deep clone, Objects are still linked
	 * @return cloned queue
	 */
	public Queue<T> clone() {
		Queue<T> clone = new Queue<T>();
		QueueNode<T> runner = firstNode;
		while (runner != null) {
			clone.enqueue(runner.value);
			runner = runner.next;
		}
		return clone;
	}

	/**
	 * Add all elements from provided source into end of
	 * queue
	 * @param source Queue of same type to add
	 */
	public void addAll(Queue<T> source) {
		Queue<T> clone = source.clone();
		while (!clone.isEmpty()) {
			enqueue(clone.dequeue());
		}
	}

	/**
	 * Returns an array of elements in Queue. Note
	 * that objects in Array will edit source objects
	 * in queue. 
	 *@return toArray Array of all elements in Queue
	 */
	@SuppressWarnings("unchecked")
	public T[] toArray() {
		T[] array = (T[]) new Object[size()];
		QueueNode<T> runner = firstNode;
		int i = 0;
		while (runner != null) {
			array[i++] = (T) runner.value;
			runner = runner.next;
		}
		return array;
	}

}
