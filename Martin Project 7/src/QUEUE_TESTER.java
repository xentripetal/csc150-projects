
/**
 * Runs a series of tests on QUEUE adt
 * @author Corbin Martin
 */
public class QUEUE_TESTER {

	/**
	 * Runs the tests
	 */
	public static void runTests() {
		constructorTest();
		isEmptyTest();
		sizeTest();
		peekLastTest();
		containsTest();
		dequeueTest();

	}

	private static void constructorTest() {
		Testing.testSection("QUEUE Constructor Test");
		Queue<Integer> q = new Queue<Integer>();

		Testing.assertEquals("Default isEmpty", true, q.isEmpty());
		Testing.assertEquals("Default is 0 size", 0, q.size());
		Testing.assertEquals("Default has no first element", null, q.dequeue());
		Testing.assertEquals("Default has no last element", null, q.peekLast());

	}

	private static void sizeTest() {
		Testing.testSection("QUEUE size Test");
		Queue<Integer> q = new Queue<Integer>();

		Testing.assertEquals("Empty queue is empty", 0, q.size());

		q.enqueue(1);
		Testing.assertEquals("singleton is not empty", 1, q.size());

		q.enqueue(2);
		q.enqueue(3);
		Testing.assertEquals("Non empty Queue", 3, q.size());

		q.dequeue();
		q.dequeue();
		q.dequeue();
		Testing.assertEquals("Modified empty is empty", 0, q.size());

	}

	private static void isEmptyTest() {
		Testing.testSection("QUEUE isEmpty Test");
		Queue<Integer> q = new Queue<Integer>();

		Testing.assertEquals("Empty queue is empty", true, q.isEmpty());

		q.enqueue(1);
		Testing.assertEquals("singleton is not empty", false, q.isEmpty());

		q.enqueue(2);
		q.enqueue(3);
		Testing.assertEquals("Non empty Queue is not empty", false, q.isEmpty());

		q.dequeue();
		q.dequeue();
		q.dequeue();
		Testing.assertEquals("Modified empty is empty", true, q.isEmpty());

	}

	private static void peekLastTest() {
		Testing.testSection("QUEUE peekLast Test");
		Queue<Integer> q = new Queue<Integer>();

		Testing.assertEquals("Empty has no last", null, q.peekLast());

		q.enqueue(1);
		Testing.assertEquals("Singleton is last", new Integer(1), q.peekLast());

		q.enqueue(2);
		q.enqueue(3);
		Testing.assertEquals("Normal last is last", new Integer(3), q.peekLast());
		
		q.dequeue();
		q.dequeue();
		Testing.assertEquals("Modified singleton is last", new Integer(3), q.peekLast());
		
		q.dequeue();
		Testing.assertEquals("Modified empty is no last", null, q.peekLast());
	}

	private static void containsTest() {
		Testing.testSection("QUEUE contains Test");
		Queue<Integer> q = new Queue<Integer>();

		Testing.assertEquals("contain on empty is false", false, q.contains(1));

		q.enqueue(1);
		Testing.assertEquals("contain on singleton nonexistant", false, q.contains(2));
		Testing.assertEquals("contain on singleton", true, q.contains(1));
		
		q.enqueue(2);
		q.enqueue(3);

		Testing.assertEquals("contain on complete tree nonexistant", false, q.contains(4));
		Testing.assertEquals("contain on complete tree", true, q.contains(3));
		q.dequeue();
		q.dequeue();
		q.dequeue();
		Testing.assertEquals("Contain on modified empty", false, q.contains(1));

	}

	private static void dequeueTest() {
		Testing.testSection("QUEUE dequeue Test");
		Queue<Integer> q = new Queue<Integer>();

		Testing.assertEquals("remove on empty is null", null, q.dequeue());
		
		q.enqueue(1);
		Testing.assertEquals("Remove on singleton", new Integer(1), q.dequeue());

		q.enqueue(1);
		q.enqueue(2);
		q.enqueue(3);
		Testing.assertEquals("Remove on normal", new Integer(1), q.dequeue());
		Testing.assertEquals("Remove updates", new Integer(2), q.dequeue());
		Testing.assertEquals("Remove updates", new Integer(3), q.dequeue());
		Testing.assertEquals("Remove on modified empty is null", null, q.dequeue());
	}
}
