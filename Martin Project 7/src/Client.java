/**
 * I affirm that all attached documents follow the Union Honor Code
 * @author Corbin Martin
 */
public class Client {
	/**
	 * Runs a series of tests on Queue and BST. Also processess US constitution
	 */
	public static void main(String[] args) {
		DataProcessor data = new DataProcessor();
		data.run("src/uscons.txt");
		Testing.startTests();
		Testing.setVerbose(true);
		BST_TESTER.runTests();
		QUEUE_TESTER.runTests();
		Testing.finishTests();
	}
}
