/**
 * Creates and manages a node for BinarySearchTree
 *
 * @author Corbin Martin
 */
public class BSTNode<K extends Comparable<K>> implements Comparable<BSTNode<K>> {

	public BSTNode<K> right;
	public BSTNode<K> left;
	public K key;

	/**
	 * Creates and manages a BSTNode
	 */
	public BSTNode() {
		right = null;
		left = null;
		key = null;
	}

	/**
	 * Creates and manages a BSTNode with a value of key
	 *
	 * @param Key
	 *            Element to store in node
	 */
	public BSTNode(K key) {
		right = null;
		left = null;
		this.key = key;
	}

	/**
	 * Compares the key of compare's key Returns 1 if key is null. Returns -1 if
	 * compare's key is null.
	 *
	 * @param compare
	 *            BSTNode to compare
	 * @return a negative integer, zero, or a positive integer as this object is
	 *         less than, equal to, or greater than compare's key
	 */
	public int compareTo(BSTNode<K> compare) {
		if (key == null)
			return 1;
		else if (compare.key == null)
			return -1;
		else
			return key.compareTo(compare.key);
	}

	/**
	 * Returns a human readable string of Key inside Node
	 */
	public String toString() {
		return "" + key;
	}

	/**
	 * @return True if Node has a right child. False if not.
	 */
	public boolean hasRightChild() {
		return this.right != null;
	}

	/**
	 * @return True if Node has a left child. False if not.
	 */
	public boolean hasLeftChild() {
		return this.left != null;
	}
}
