/**
 * Creates and Manages a Node wrapper for a value
 * @author Corbin Martin
 * Invariants
 * 1. Next points to another QueueNode in the Queue chain.
 */
public class QueueNode<T> {
	public QueueNode<T> next;
	public T value;

	/**
	 * Constructs an empty Node with no value or next
	 */
	public QueueNode() {
		value = null;
		next = null;
	}

	/**
	 * @param value Internal value to store
	 */
	public QueueNode(T value) {
		this.value = value;
		next = null;
	}

	/**
	 * Returns a human readable string of the nodes value
	 */
	public String toString() {
		return "" + value;
	}

}
