
/**
 * Constructs a dictionary and index of a provided text file
 * @author Corbin Martin
 */
public class DataProcessor {
	private final String NEWLINE_TOKEN = "#";

	private BinarySearchTree<Indice> index;
	private BinarySearchTree<String> dictionary;

	/**
	 * Constructs the DataProcessor
	 */
	public DataProcessor() {
		index = new BinarySearchTree<Indice>();
		dictionary = new BinarySearchTree<String>();
	}

	/**
	 * Runs the DataProcessor on the provided file.
	 * Note that it stores Indices and dictionary words from
	 * previous runs.
	 */
	public void run(String sourceFile) {
		FileReader reader = new FileReader(sourceFile);
		String input = reader.nextToken();
		int currentPage = 1;

		while (input != null) {
			input = input.toLowerCase();

			if (input.length() >= 3 && !dictionary.contains(input))
				sortInput(input, currentPage);
			else if (input.equals(NEWLINE_TOKEN))
				currentPage++;
			
			input = reader.nextToken();
		}

		System.out.println("DICTIONARY:\n" + dictionary.inOrder());
		System.out.println("INDEX:\n" + index.inOrder());
	}

	private void sortInput(String input, int currentPage) {
		Indice match = index.peek(new Indice(input));

		if (match == null) {
			Indice newIndice= new Indice(input);
			newIndice.insertPage(currentPage);
			index.insert(newIndice);
		}
		else if (!match.insertPage(currentPage))
			swapIndexToDict(match);
	}		

	private void swapIndexToDict(Indice match) {
		System.out.println(match);
		index.remove(match);
		dictionary.insert(match.getWord());		
	}
}
