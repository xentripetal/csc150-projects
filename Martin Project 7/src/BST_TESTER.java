/**
 * Runs a series of tests on BST adt
 * @author Corbin Martin
 */
public class BST_TESTER {
	
	/**
	 * Runs the tests
	 */
	public static void runTests() {

		constructorTest();
		sizeTest();
		removeTest();
		peekTest();
		containsTest();
		toStringTest();
		inOrderTest();
	}

	private static void constructorTest() {
		Testing.testSection("BST Constructor Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();
		Testing.assertEquals("Constructor is empty", 0, bst.size());
		Testing.assertEquals("toString Constructor is empty", "", bst.toString());
	}

	private static void sizeTest() {
		Testing.testSection("BST size Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();

		Testing.assertEquals("Empty is Empty", 0, bst.size());

		bst.insert(1);
		Testing.assertEquals("Singleton is 1", 1, bst.size());

		bst.insert(2);
		bst.insert(3);
		Testing.assertEquals("Full tree is 3", 3, bst.size());

		bst.insert(-1);
		bst.insert(-2);
		Testing.assertEquals("Multi tree is 5", 5, bst.size());

		bst.remove(3);
		bst.remove(2);
		bst.remove(-1);
		Testing.assertEquals("Remove updates size", 2, bst.size());

		bst.remove(-2);
		bst.remove(1);
		Testing.assertEquals("Modified empty is empty", 0, bst.size());
	}

	private static void removeTest() {
		Testing.testSection("BST remove Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();

		Testing.assertEquals("Remove on empty is null", null, bst.remove(1));

		bst.insert(0);
		Testing.assertEquals("Remove on singleton nonexistant", null, bst.remove(2));
		Testing.assertEquals("Remove on singleton", new Integer(0), bst.remove(0));
		
		bst.insert(0);
		bst.insert(-1);
		bst.insert(1);

		Testing.assertEquals("Remove on complete tree nonexistant", null, bst.remove(4));
		Testing.assertEquals("Remove on complete tree", new Integer(0), bst.remove(0));
		Testing.assertEquals("Remove actually removes", null, bst.remove(0));

		bst.insert(5);
		bst.insert(6);
		bst.insert(4);
		Testing.assertEquals("Remove on normal tree nonexistant", null, bst.remove(8));
		Testing.assertEquals("Remove on normal tree", new Integer(4), bst.remove(4));
	}

	private static void toStringTest() {
		Testing.testSection("BST toString Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();
		Testing.assertEquals("toString on empty", "", bst.toString());

		bst.insert(0);
		Testing.assertEquals("toString on singleton", "( 0 )", bst.toString());
		
		bst.insert(1);
		bst.insert(-1);

		Testing.assertEquals("toString on complete tree", "(( -1 ) 0 ( 1 ))", bst.toString());

		bst.insert(5);
		bst.insert(6);
		bst.insert(7);
		Testing.assertEquals("toString on normal tree", "(( -1 ) 0 ( 1 ( 5 ( 6 ( 7 )))))", bst.toString());
	}

	private static void inOrderTest() {
		Testing.testSection("BST inOrder Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();
		Testing.assertEquals("inOrder on empty", "", bst.inOrder());

		bst.insert(0);
		Testing.assertEquals("inOrder on singleton", "0\n", bst.inOrder());
		
		bst.insert(1);
		bst.insert(-1);

		Testing.assertEquals("inOrder on complete tree", "-1\n0\n1\n", bst.inOrder());

		bst.insert(5);
		bst.insert(6);
		bst.insert(7);
		Testing.assertEquals("inOrder on normal tree", "-1\n0\n1\n5\n6\n7\n", bst.inOrder());

	}

	private static void peekTest() {
		Testing.testSection("BST peek Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();

		Testing.assertEquals("peek on empty is null", null, bst.peek(1));

		bst.insert(1);
		Testing.assertEquals("peek on singleton nonexistant", null, bst.peek(2));
		Testing.assertEquals("peek on singleton", new Integer(1), bst.peek(1));
		
		bst.insert(2);
		bst.insert(3);

		Testing.assertEquals("peek on complete tree nonexistant", null, bst.peek(4));
		Testing.assertEquals("peek on complete tree", new Integer(3), bst.peek(3));

		bst.insert(5);
		bst.insert(6);
		bst.insert(7);
		Testing.assertEquals("peek on normal tree nonexistant", null, bst.peek(8));
		Testing.assertEquals("peek on normal tree", new Integer(7), bst.peek(7));
	}

	private static void containsTest() {
		Testing.testSection("BST Contains Tests");
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();

		Testing.assertEquals("contain on empty is false", false, bst.contains(1));

		bst.insert(1);
		Testing.assertEquals("contain on singleton nonexistant", false, bst.contains(2));
		Testing.assertEquals("contain on singleton", true, bst.contains(1));
		
		bst.insert(2);
		bst.insert(3);

		Testing.assertEquals("contain on complete tree nonexistant", false, bst.contains(4));
		Testing.assertEquals("contain on complete tree", true, bst.contains(3));

		bst.insert(5);
		bst.insert(6);
		bst.insert(7);
		Testing.assertEquals("contain on normal tree nonexistant", false, bst.contains(8));
		Testing.assertEquals("contain on normal tree", true, bst.contains(7));
	}
}
