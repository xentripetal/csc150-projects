/**
 * Creates and manages and Indice element for DataProcessor
 *
 * @author Corbin Martin
 */
public class Indice implements Comparable<Indice> {
	private String word;
	private Queue<Integer> pages;

	/**
	 * Constructs a Indice with the provided word
	 */
	public Indice(String word) {
		this.word = word;
		pages = new Queue<Integer>();
	}

	/**
	 * Getter for stored Word of indice.
	 */
	public String getWord() {
		return word;
	}

	/**
	 * Inserts the provided page. If the page already exists it will not be
	 * inserted. If there are atleast 4 pages it will not be inserted
	 *
	 * @param page
	 *            Page number to insert
	 * @return Returns True if the page was inserted or already existed. Returns
	 *         false if the pagelist is full.
	 */
	public boolean insertPage(int page) {
		Integer compare = pages.peekLast();
		if (compare != null && !compare.equals(page)) {
			if (pages.size() >= 4)
				return false;
			else 
				pages.enqueue(page);
		}
		else if (compare == null)
			pages.enqueue(page);
		return true;
	}

	/**
	 * Returns a human readable string of the Indice
	 */
	public String toString() {
		String concatString = getWord() + " ";
		while (!pages.isEmpty()) {
			concatString += pages.dequeue();
			if (!pages.isEmpty())
				concatString += ", ";
		}
		return concatString;
	}

	/**
	 * Compares the indice to compare
	 *
	 * @param compare
	 *            Indice to compare
	 * @return a negative integer, zero, or a positive integer as this object is
	 *         less than, equal to, or greater than compare
	 */
	public int compareTo(Indice compare) {
		return getWord().compareTo(compare.getWord());
	}
}
