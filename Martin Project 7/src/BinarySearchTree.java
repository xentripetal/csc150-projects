/**
 * Creates and manages a Binary Search Tree with no duplicates
 * @author Corbin Martin
 * Invariants 
 * 1. If root is null, the tree is empty 
 * 2. If root is not null it is the top item of the tree
 */
public class BinarySearchTree<K extends Comparable<K>> {

	private BSTNode<K> root;

	/**
	 * Creates a BinarySearchTree
	 */
	public BinarySearchTree() {
		root = null;
	}

	/**
	 * @return number of elements in BinarySearchTree
	 */
	public int size() {
		return size(root);
	}

	private int size(BSTNode<K> N) {
		if (N == null)
			return 0;
		else
			return size(N.left) + size(N.right) + 1;
	}

	/**
	 * Inserts the provided key into the tree
	 *
	 * @param Key
	 *            key to insert.
	 */
	public void insert(K key) {
		root = insert(root, new BSTNode<K>(key));
	}

	/**
	 * Returns the element matching key in tree.
	 * Returns null if does not exist
	 * @param Key
	 *						Key to match
	 * @return The element matching key.
	 */
	public K peek(K key) {
		BSTNode<K> match = getMatchingNode(root, new BSTNode<K>(key));
		if (match == null)
			return null;
		else
			return match.key;
	}

	private BSTNode<K> insert(BSTNode<K> subroot, BSTNode<K> newValue) {
		if (subroot == null)
			subroot = newValue;
		else if (newValue.compareTo(subroot) > 0)
			subroot.right = insert(subroot.right, newValue);
		else if (newValue.compareTo(subroot) < 0)
			subroot.left = insert(subroot.left, newValue);
		return subroot;
	}

	/**
	 * Checks if tree contains check
	 *
	 * @param check
	 *            Key to search for
	 * @return True if found, False if not
	 */
	public boolean contains(K check) {
		return (getMatchingNode(root, new BSTNode<K>(check)) != null);
	}

	private BSTNode<K> getMatchingNode(BSTNode<K> subroot, BSTNode<K> check) {
		if (subroot == null)
			return null;
		else if (check.compareTo(subroot) > 0)
			return getMatchingNode(subroot.right, check);
		else if (check.compareTo(subroot) < 0)
			return getMatchingNode(subroot.left, check);
		else
			return subroot;
	}

	/**
	 * Removes the element matching K. Returns null if no element matches K. :wq
	 *
	 * @param check
	 *            Element to look for
	 * @return Matching element that was removed
	 */
	public K remove(K check) {
		// Takes an additional log(n) runtime but no way to collect key without
		// either doing this or keeping a floating private value of
		// lastOperation
		// which would break with recursion anyway
		// if nodes stored parents you could delete from getMatch
		BSTNode<K> toRemove = getMatchingNode(root, new BSTNode<K>(check));

		K removeKey;
		if (toRemove == null)
			removeKey = null;
		else {
			removeKey = toRemove.key;
			root = remove(root, toRemove);
		}

		return removeKey;
	}

	private BSTNode<K> remove(BSTNode<K> subroot, BSTNode<K> compare) {
		if (subroot == null)
			return null;
		else if (compare.compareTo(subroot) > 0)
			subroot.right = remove(subroot.right, compare);
		else if (compare.compareTo(subroot) < 0)
			subroot.left = remove(subroot.left, compare);
		else
			subroot = executeRemove(subroot);

		return subroot;
	}

	private BSTNode<K> executeRemove(BSTNode<K> subroot) {
		if (!subroot.hasLeftChild())
			return subroot.right;
		else if (!subroot.hasRightChild())
			return subroot.left;
		else {
			BSTNode<K> minNode = getMinNode(subroot.right);
			subroot.key = minNode.key;
			subroot.right = remove(subroot.right, subroot);
		}
		return subroot;
	}

	/**
	 * Returns the smallest node in the provided subroot.
	 */
	private BSTNode<K> getMinNode(BSTNode<K> subroot) {
		if (subroot == null)
			return null;
		BSTNode<K> minNode = subroot;
		while (minNode.left != null) {
			minNode = minNode.left;
		}
		return minNode;
	}
	
	/**
	 * Returns a human readable string of the BinarySearchTree
	 */
	public String inOrder() {
		return inOrder(root);
	}

	private String inOrder(BSTNode<K> subroot) {
		if (subroot == null)
			return "";
		else {
			String concatString = inOrder(subroot.left);
			concatString += subroot.toString() + "\n";
			concatString += inOrder(subroot.right);
			return concatString;
		}
	}

	/**
	 * Returns a human readable string of the BinarySearchTree
	 */
	public String toString() {
		return toString(root);
	}

	private String toString(BSTNode<K> subroot) {
		if (subroot == null)
			return "";
		else {
			String concatString = "(" + toString(subroot.left);
			concatString += " " + subroot.toString() + " ";
			concatString += toString(subroot.right) + ")";
			return concatString;
		}
	}
}
