/**
 *I affirm that I have carried out the attached academic endeavors with full academic honesty,
 * in accordance with the Union College Honor Code and the course syllabus.
 * Creates and manages a sequence object
 * @author Corbin Martin
 *
 * Invariant
 * 1. Capacity is Always equal to or greater than size.
 * 2. Size is always greater than or equal to 0.
 * 3. Current is always greater than or equal to -1 or less than size(). 
 */
public class Sequence
{
	private final int DEFAULT_CAPACITY = 10;
	private LinkedList data;
	private int capacity;
	private int size;
	private int current;
 
    /**
     * Creates a new sequence with initial capacity 10.
     */
    public Sequence() {
    	data = new LinkedList();
    	capacity = 10;
    	size = 0;
    	current = -1;
    }
    

    /**
     * Creates a new sequence.
     * 
     * @param initialCapacity the initial capacity of the sequence.
     */
    public Sequence(int initialCapacity){
    	data = new LinkedList();
    	capacity = initialCapacity;
    	size = 0;
    	current = -1;
    }
    

    /**
     * Adds a string to the sequence in the location before the
     * current element. If the sequence has no current element, the
     * string is added to the beginning of the sequence.
     *
     * The added element becomes the current element.
     *
     * If the sequences's capacity has been reached, the sequence will
     * expand to twice its current capacity plus 1.
     *
     * @param value the string to add.
     */
    public void addBefore(String value)
    {
    	int pushIndex = current;
    	
    	if (!isCurrent()) {
    		pushIndex = 0;
    		current = 0;
    	}
    	
    	checkAndPushCapacity();
    	pushItemsForward(pushIndex);
    	data.insert(pushIndex, value);
    }
    
    /**
     * Adds a string to the sequence in the location after the current
     * element. If the sequence has no current element, the string is
     * added to the end of the sequence.
     *
     * The added element becomes the current element.
     *
     * If the sequences's capacity has been reached, the sequence will
     * expand to twice its current capacity plus 1.
     *
     * @param value the string to add.
     */
    public void addAfter(String value)
    {
    	int pushIndex = current + 1;
    	
    	if (!isCurrent()) {
    		pushIndex = size();
    		current = size();
    	} else {
    		current++;
    	}
    	
    	checkAndPushCapacity();
    	pushItemsForward(pushIndex);
    	data.insert(pushIndex, value);
    }
    
    /**
     * Only useful for a single item add. Using for more than 1 item
     * can result in a unsuitable capacity. 
     */
    private void checkAndPushCapacity()
    {
    	if (size() + 1 > getCapacity()) {
    		setCapacity(2 * size() + 1);
    	}
    }
    
    
    /**
     * Push all items at and past pushIndex forward by 1. Assumes safe
     * capacity is available, if not data will be lost
     * @param pushIndex index in data to push from
     */
    private void pushItemsForward(int pushIndex) 
    {
    	size++;
    }
    
    private void pushItemsBackwards(int pushIndex)
    {
    	data.remove(pushIndex-1);
    	size--;
    }

    
    /**
     * @return true if and only if the sequence has a current element.
     */
    public boolean isCurrent()
    {
    	if (current < size() && current >= 0)
    		return true;
    	
    	return false;
    }
    
    
    /**
     * @return the capacity of the sequence.
     */
    public int getCapacity()
    {
    	return capacity;
    }

    
    /**
     * @return the element at the current location in the sequence, or
     * null if there is no current element.
     */
    public String getCurrent()
    {
    	if (isCurrent())
    		return peekIndex(current);
    	else
    		return null;
    }
    
    
    /**
     * Increase the sequence's capacity to be
     * at least minCapacity.  Does nothing
     * if current capacity is already >= minCapacity.
     *
     * @param minCapacity the minimum capacity that the sequence
     * should now have.
     */
    public void ensureCapacity(int minCapacity)
    {
    	if (getCapacity() < minCapacity) {
    		setCapacity(minCapacity);
    	}
    }
    
    /**
     * Sets the sequence's capacity to exactly
     * the given capacity. Will copy all data up
     * to that point in sequential order.
     * 
     * @param capacity The exact capacity that the sequence 
     * should now have.
     */
    private void setCapacity(int capacity) {
    	this.capacity = capacity;
    }

    
    /**
     * Places the contents of another sequence at the end of this sequence.
     *
     * If adding all elements of the other sequence would exceed the
     * capacity of this sequence, the capacity is changed to make room for
     * all of the elements to be added.
     * 
     * Postcondition: NO SIDE EFFECTS!  the other sequence should be left
     * unchanged.  The current element of both sequences should remain
     * where they are. (When this method ends, the current element
     * should refer to the same element that it did at the time this method
     * started.)
     *
     * @param another the sequence whose contents should be added.
     */
    public void addAll(Sequence another)
    {
    	int cachedPosition = current;
    	Sequence cloneOther = another.clone();
    	current = size() - 1;
    	
    	ensureCapacity(size() + cloneOther.size());
    	cloneOther.start();
    	while(cloneOther.isCurrent()) {
    		this.addAfter(cloneOther.getCurrent());
    		cloneOther.advance();
    	}
    	
    	current = cachedPosition;
    }

    
    /**
     * Move forward in the sequence so that the current element is now
     * the next element in the sequence.
     *
     * If the current element was already the end of the sequence,
     * then advancing causes there to be no current element.
     *
     * If there is no current element to begin with, do nothing.
     */
    public void advance()
    {
    	if (isCurrent()) {
    		current++;
    		if (!isCurrent())
    			current = -1;
    	}
    }

    
    /**
     * Make a copy of this sequence.  Subsequence changes to the copy
     * do not affect the current sequence, and vice versa.
     * 
     * Postcondition: NO SIDE EFFECTS!  This sequence's current
     * element should remain unchanged.  The clone's current
     * element will correspond to the same place as in the original.
     *
     * @return the copy of this sequence.
     */
    public Sequence clone()
    {
    	Sequence clone = new Sequence(getCapacity());
    	
    	for (int i = 0; i < size(); i++) {
    		clone.addAfter(peekIndex(i));
    	}
    	
    	
    	if (isCurrent()) {
    		clone.start();
    		for (int i = 0; i < current; i++)
    			clone.advance();
    	}
    	else {
    		//Else advance past last, setting to no current
    		clone.advance();
    	}
    	
    	return clone;
    }
   
    
    /**
     * Remove the current element from this sequence.  The following
     * element, if there was one, becomes the current element.  If
     * there was no following element (current was at the end of the
     * sequence), the sequence now has no current element.
     *
     * If there is no current element, does nothing.
     */
    public void removeCurrent()
    {
    	if (isCurrent())
    		pushItemsBackwards(current+1);
    	
    	if (!isCurrent())
    		current = -1;
    }

    
    /**
     * @return the number of elements stored in the sequence.
     */
    public int size()
    {
    	return size;
    }

    
    /**
     * Sets the current element to the start of the sequence.  If the
     * sequence is empty, the sequence has no current element.
     */
    public void start()
    {
    	if (size() > 0)
    		current = 0;
    	else
    		current = -1;
    }

    
    /**
     * Reduce the current capacity to its actual size, so that it has
     * capacity to store only the elements currently stored.
     */
    public void trimToSize()
    {
    	setCapacity(size());
    }
    
    
    /**
     * Produce a string representation of this sequence.  The current
     * location is indicated by a >.  For example, a sequence with "A"
     * followed by "B", where "B" is the current element, and the
     * capacity is 5, would print as:
     * 
     *    {A, >B} (capacity = 5)
     * 
     * The string you create should be formatted like the above example,
     * with a comma following each element, no comma following the
     * last element, and all on a single line.  An empty sequence
     * should give back "{}" followed by its capacity.
     * 
     * @return a string representation of this sequence.
     */
    public String toString() 
    {
    	String concatString = "{";
    	for (int i = 0; i < size(); i++) {
    		if (i == current)
    			concatString += ">";
    		
    		concatString += peekIndex(i);
    		
    		if (i < size()-1) 
    			concatString += ", ";
    	}
    	
    	concatString += "} (capacity = " + getCapacity() + ")";
    	return concatString;
    }
    
    /**
     * Checks whether another sequence is equal to this one.  To be
     * considered equal, the other sequence must have the same size
     * as this sequence, have the same elements, in the same
     * order, and with the same element marked
     * current.  The capacity can differ.
     * 
     * Postcondition: NO SIDE EFFECTS!  this sequence and the
     * other sequence should remain unchanged, including the
     * current element.
     * 
     * @param other the other Sequence with which to compare
     * @return true iff the other sequence is equal to this one.
     */
    public boolean equals(Sequence other) 
    {
    	if (other.size != this.size())
    		return false;
    	
    	if (other.isCurrent() != this.isCurrent())
    		return false;
    	
    	Sequence cloneCompare = other.clone();
    	Sequence cloneSelf = clone();
    	
    	
    	//Check from current position to end and make sure all matches
    	int currentTillEnd = 0;
    	while (cloneCompare.isCurrent()) {
    		if (!(cloneCompare.getCurrent().equals(cloneSelf.getCurrent())))
    			return false;
    	
    		cloneCompare.advance();
    		cloneSelf.advance();
    		currentTillEnd++;
    	}
    	
    	//If Self current has an item then they are not equal
    	if (cloneSelf.isCurrent())
    		return false;
    	
    	cloneCompare.start();
    	cloneSelf.start();
    	
    	//Check from beginning to previous position for matching
    	for (int i = 0; i < cloneCompare.size - currentTillEnd; i++) {
    		if (!(cloneCompare.getCurrent().equals(cloneSelf.getCurrent())))
    			return false;
    		
    		cloneCompare.advance();
    		cloneSelf.advance();
    	}
    	
    	return true;
    }
    
    private String peekIndex(int i) {
    	return (String) data.peek(i);
    }
    
    
    /**
     * 
     * @return true if Sequence empty, else false
     */
    public boolean isEmpty()
    {
    	return (size() == 0);
    }
    
    
    /**
     *  empty the sequence.  There should be no current element.
     */
    public void clear()
    {	
    	size = 0;
    	current = -1;
		//While not neccesary to clear data, it saves memory
    	data.clear();
    }

}
