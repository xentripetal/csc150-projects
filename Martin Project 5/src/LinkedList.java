/**
 * Linked List is a collection of data nodes.  All methods here relate to
 * how one can manipulate those nodes.
 * 
 * @author Corbin Martin
 * @version 02-09-2017
 */
public class LinkedList
{
    private int length;          // number of nodes
    private ListNode firstNode;  // pointer to first node

    /**
     * Creates a empty LinkedList.
     */
    public LinkedList()
    {
        length = 0;
        firstNode = null;
    }
    
	/**
	 * Returns whether the linked list is empty
	 * @return Boolean argument, true if empty false if not
	 */
    public boolean isEmpty() {
    	return (getLength() == 0);
    }
    
    /**
     * Removes the Event at head of LinkedList and returns
     * the Event. 
     * @return The removed event at head. null if empty
     */
	public Object removeHead() {
		return remove(0);
    }
    
	/**
	 * Removes all elements in LinkedList and resets length
	 */
	public void clear() {
		firstNode = null;
		length = 0;
	}
    /**
     * Inserts event at the end of the LinkedList
     * @param event The event to insert
     */
    public void insertAtTail(Object newData) {
    	insert(getLength(), newData);
    }
    
    /**
     * Inserts the given insert at the requested index.
     * Can throw IndexOutOfBounds
     * @param index The supplied index to insert at
     * @param insert Object to insert at index
     */
    public void insert(int index, Object insert) {
    	if (index < 0 || index > getLength())
    		throw new IndexOutOfBoundsException();

		ListNode newInsert = new ListNode(insert);
		
		if (index == 0) {
			if (!isEmpty())
				newInsert.next = firstNode;
			firstNode = newInsert;
		} else {
			ListNode parent = getNode(index - 1);
			newInsert.next = parent.next;
			parent.next = newInsert;
		}
		length++;
    }

	private ListNode getNode(int index) { 
    	if (index < 0 || index >= getLength())
    		throw new IndexOutOfBoundsException("Internal Out Of Bounds");
		
			ListNode runner = firstNode;
			for (int i = 0; i < index; i++)
				runner = runner.next;

			return runner;
		}
    
    /**
	 * Returns object at the requested index
	 * Can throw IndexOutOfBounds
	 * @param index Index of object to see
	 * @return The object at the requested index
	 */
	public Object peek(int index) {
    	if (index < 0 || index >= getLength())
    		throw new IndexOutOfBoundsException();
    		
    	return getNode(index).content; 
    }
    
	/**
	 * Removes and returns object at requested index
	 * Can throw IndexOutOfBounds
	 * @param index Index of object to remove
	 * @return Object that was removed at index
	 */
    public Object remove(int index) {
    	if (index < 0 || index >= getLength())
    		throw new IndexOutOfBoundsException();

		Object toReturn = null;
		if (index == 0) {
			toReturn = firstNode.content;
			if (getLength() == 1)
				firstNode = null;
			else 
				firstNode = firstNode.next;
		} else {
			ListNode parent = getNode(index - 1);
			toReturn = parent.next.content;
			parent.next = parent.next.next;
		}
		
		length--;
		return toReturn;
			
    }
    
    /**
     * Gets the last occuring index matching compare
     * return is -1 if does not exist
     * @param compare Object to find
     * @return last index of compare
     */
    public int getLastIndex(Object compare) {
    	ListNode runner = firstNode;
    	int index = 0;
    	int candidate = -1;
    	while (runner != null) {
    		if (compare.equals(runner.content))
    			candidate = index;
    		index++;
    		runner = runner.next;
    	}
    	return candidate;
    }
    
    /**
     * Gets the first occuring index matching compare
     * return is -1 if does not exist.
     * @param compare Object to find
     * @return first index of compare
     */
    public int getFirstIndex(Object compare) {
    	ListNode runner = firstNode;
    	int index = 0;
    	while (runner != null) {
    		if (compare.equals(runner.content))
    			return index;
    		else {
    			runner = runner.next;
    			index++;
    		}
    	}
    	return -1;
    }
    
    
    /** insert new Event at linked list's head
     * 
     * @param newData the Event to be inserted
     */
    public void insertAtHead(Object newData)
    {
    	insert(0, newData);
    }
    
    /** Turn entire chain into a string
     *  
     *  @return return linked list as printable string
     */
    public String toString() 
    {
    	String toReturn="(";
    	ListNode n = firstNode;
    	
    	while (n != null) {
    		toReturn += n;
    		n = n.next;
    		if (n!=null)
    			toReturn = toReturn + ",\n";
    	}
    	
    	toReturn = toReturn + ")";
    	return toReturn;
    }
    
    /** getter for number of nodes in the linked list
     * 
     * @return length of LL
     */
    public int getLength() {
    	return length;
	}
}
