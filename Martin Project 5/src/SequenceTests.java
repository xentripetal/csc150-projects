/**
 *  Runs a series of tests on Sequence object
 * @author Corbin Martin
 * @version 2/20/2017
 */
public class SequenceTests {
    
    public static void main(String[] args)
    {
    	Testing.setVerbose(true); // use false for less testing output
		Testing.startTests();
    	testCreate();
    	testToString();
    	testAddAfter();
    	testAddBefore();
    	testRemoveCurrent();
    	testAdvance();
    	testIsCurrent();
    	testIsEmpty();
    	testClear();
    	testGetCurrent();
    	testGetCapacity();
    	testEquals();
    	testClone();
    	testTrimToSize();
    	testStart();
    	testSize();
    	testAddAll();
    	testEnsureCapacity();
	
    	Testing.finishTests();
    	
    }
    
    private static void testClear() {
    	Testing.testSection("Testing clear");
    	
    	Sequence s1 = new Sequence();
    	s1.clear();
    	Testing.assertEquals("Empty cleared is still empty", true, s1.isEmpty());
    	
    	s1.addAfter("a");
    	s1.clear();
    	Testing.assertEquals("Normal sequence (non full) is emptied", true, s1.isEmpty());
    	Testing.assertEquals("Cleared sequence has no current", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	s1.trimToSize();
    	s1.clear();
    	Testing.assertEquals("Sequence at capacity is emptied", true, s1.isEmpty());
    	
    	Sequence s2 = new Sequence(0);
    	s2.clear();
    	Testing.assertEquals("No capacity sequence is emptied", true, s2.isEmpty());
    }
    
    private static void testIsEmpty() {
    	Testing.testSection("Testing isEmpty");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("Default is empty", true, s1.isEmpty());
    	
    	s1.addAfter("a");
    	Testing.assertEquals("Default with value is not empty", false, s1.isEmpty());
    	
    	s1.addAfter("b");
    	s1.start();
    	s1.removeCurrent();
    	Testing.assertEquals("Non empty with removed is not empty", false, s1.isEmpty());
    	
    	s1.removeCurrent();
    	Testing.assertEquals("Sequeunce with all removed values is empty", true, s1.isEmpty());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("No capacity sequence is empty", true, s2.isEmpty());
    }
    
    private static void testEquals() {
    	Testing.testSection("Testing equals");
    	
    	Sequence s1 = new Sequence();
    	Sequence s2 = new Sequence();
    	Sequence s3 = new Sequence(5);
    	Testing.assertEquals("Equals with two empty", true, s1.equals(s2));
    	
    	s1.addAfter("a");
    	Testing.assertEquals("Not equals with values and no values", false, s1.equals(s2));
    	
    	s2.addAfter("a");
    	Testing.assertEquals("Equals with values in same order", true, s1.equals(s2));
    	
    	s1.advance();
    	Testing.assertEquals("Not equals with one with current one without", false, s1.equals(s2));
    	
    	s2.advance();
    	Testing.assertEquals("Equals with both no current value", true, s1.equals(s2));
    	
    	s1.start();
    	s1.addAfter("b");
    	s1.start();
    	Testing.assertEquals("Not Equals with different values", false, s1.equals(s2));
    	
    	s2.addBefore("b");
    	Testing.assertEquals("Not equals with different order", false, s1.equals(s2));
    	
    	s2.removeCurrent();
    	s2.addAfter("b");
    	Sequence s2Clone = s2.clone();
    	Sequence s1Clone = s1.clone();
    	Testing.assertEquals("Not Equals with different currents", false, s1.equals(s2));
    	Testing.assertEquals("Inputted Sequence unmodified", true, s2.equals(s2Clone));
    	Testing.assertEquals("Source sequeucne unmodified", true, s1.equals(s1Clone));
    	
    	s3.addAfter("a");
    	s3.addAfter("b");
    	s3.start();
    	Testing.assertEquals("Equals with different capacities", true, s1.equals(s3));
    	
    	Sequence s4 = new Sequence(0);
    	Testing.assertEquals("Not equals when compared with no capacity", false, s1.equals(s4));
    	Sequence s5 = new Sequence();
    	Testing.assertEquals("Equals when no capacity and default", true, s4.equals(s5));
    	
    }
    
    private static void testToString() {
    	Testing.testSection("Testing toString");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("toString on default", "{} (capacity = 10)", s1.toString());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	Testing.assertEquals("Normal situation toString", "{a, >b} (capacity = 10)", s1.toString());
    	
    	s1.advance();
    	Testing.assertEquals("Current marked correctly past last", "{a, b} (capacity = 10)", s1.toString());
    	
    	s1.trimToSize();
    	Testing.assertEquals("Capacity correctly marked & full test", "{a, b} (capacity = 2)", s1.toString());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("Valid on no capacity", "{} (capacity = 0)", s2.toString());
    }
    
    private static void testTrimToSize() {
    	Testing.testSection("Testing trimToSize");
    	
    	Sequence s1 = new Sequence();
    	s1.trimToSize();
    	Testing.assertEquals("Trim to empty", 0, s1.getCapacity());
    	
    	s1.trimToSize();
    	Testing.assertEquals("Trim on no capacity", 0, s1.getCapacity());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	s1.trimToSize();
    	Testing.assertEquals("Trim to normal", 2, s1.getCapacity());
    	
    	
    }
    
    private static void testStart() {
    	Testing.testSection("Testing start");
    	
    	Sequence s1 = new Sequence();
    	Sequence s1Clone = s1.clone();
    	s1.start();
    	Testing.assertEquals("No changes on empty set", true, s1.equals(s1Clone));
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	s1.start();
    	Testing.assertEquals("Valid start position", "a", s1.getCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	s2.start();
    	Testing.assertEquals("No effects on starting on no capacity", false, s2.isCurrent());
    	
    	s1.trimToSize();
    	s1.start();
    	Testing.assertEquals("proper start on full capacity", "a", s1.getCurrent());
    }
    
    private static void testSize() {
    	Testing.testSection("Testing size");
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("No size on default", 0, s1.size());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	Testing.assertEquals("valid size after add", 2, s1.size());
    	
    	s1.removeCurrent();
    	Testing.assertEquals("valid size after remove", 1, s1.size());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("No size on no capacity", 0, s2.size());
    	
    	Sequence s3 = new Sequence(2);
    	s3.addAfter("a");
    	s3.addAfter("b");
    	Testing.assertEquals("valid size on full capacity", 2, s3.size());
    }
    
    private static void testRemoveCurrent() {
    	Testing.testSection("Testing removeCurrent");
    	Sequence s1 = new Sequence();
    	
    	s1.start();
    	s1.removeCurrent();
    	Testing.assertEquals("remove on nothing", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	s1.addBefore("b");
    	s1.removeCurrent();
    	Testing.assertEquals("Remove current in normal situation",
    			"{>a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("valid size after remove", 1, s1.size());
    	
    	s1.advance();
    	s1.removeCurrent();
    	Testing.assertEquals("Remove past last item", "{a} (capacity = 10)", s1.toString());
    	
    	s1.start();
    	s1.removeCurrent();
    	Testing.assertEquals("Remove only remaining item", "{} (capacity = 10)", s1.toString());
    	
    	Sequence s2 = new Sequence(0);
    	s2.start();
    	s2.removeCurrent();
    	Testing.assertEquals("Remove on no capacity", "{} (capacity = 0)", s2.toString());
    }
    
    private static void testClone() {
    	Testing.testSection("Testing Clone");
    	Sequence s1 = new Sequence();
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	
    	Sequence s2 = s1.clone();
    	Testing.assertEquals("Clone produces copy", true, s1.equals(s2));
    	
    	s1.addAfter("c");
    	Testing.assertEquals("Clone and parent are independent", false, s1.equals(s2));
    	
    	Sequence s3 = new Sequence(0);
    	Sequence s4 = s3.clone();
    	Testing.assertEquals("Clone of empty capacity", true, s3.equals(s4));
    	
    	Sequence s5 = new Sequence(2);
    	s5.addAfter("a");
    	s5.addAfter("b");
    	Sequence s6 = s5.clone();
    	Testing.assertEquals("Clone of full capacity", true, s5.equals(s6));
    	
    	s5.advance();
    	Sequence s7 = s5.clone();
    	Testing.assertEquals("Clone with no current", s5.toString(), s7.toString());
    	
    }
    
    private static void testAdvance() {
    	Testing.testSection("Testing advance");
    	
    	Sequence s1 = new Sequence();
    	
    	s1.advance();
    	Testing.assertEquals("Advance on empty still empty", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	s1.addBefore("b");
    	s1.advance();
    	Testing.assertEquals("Advance on normal set", "a", s1.getCurrent());
    	
    	s1.advance();
    	Testing.assertEquals("Advance past last is empty", false, s1.isCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	s2.advance();
    	Testing.assertEquals("Advance on no capacity", false, s2.isCurrent());
    	
    	Sequence s3 = new Sequence(1);
    	s3.addAfter("a");
    	s3.advance();
    	Testing.assertEquals("Advance at end of capacity", false, s3.isCurrent());
    }
    
    private static void testAddAll() {
    	Testing.testSection("Testing addAll");
    	Sequence s1 = new Sequence();
    	Sequence s2 = new Sequence();
    	
    	s2.addAfter("a");
    	s2.addAfter("b");
    	s2.addAfter("c");
    	s2.start();
    	s2.advance();
    	
    	Sequence s2Clone = s2.clone();
    	s1.addAll(s2);
    	Testing.assertEquals("Add to empty", "{a, b, c} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Current is still none", false, s1.isCurrent());
    	Testing.assertEquals("Added sequence is unchanged", true, s2Clone.equals(s2));
    	Testing.assertEquals("valid size after add to empty", 3, s1.size());
    	
    	Sequence s3 = new Sequence(2);
    	s3.addAfter("d");
    	s3.addAfter("e");
    	s3.addAll(s2);
    	Testing.assertEquals("Proper add to full capacity", "{d, >e, a, b, c} (capacity = 5)",
    			s3.toString());
    	Testing.assertEquals("valid size after add to full capacity", 5, s3.size());
    	
    	Sequence s4 = new Sequence(0);
    	s3.addAll(s4);
    	Testing.assertEquals("Add null sequence", "{d, >e, a, b, c} (capacity = 5)",
    			s3.toString());
    	Testing.assertEquals("valid size after add null sequence", 5, s3.size());
    	
    	s4.addAll(s3);
    	Testing.assertEquals("Add to null sequence", "{d, e, a, b, c} (capacity = 5)",
    			s4.toString());
    	Testing.assertEquals("valid size after add to null sequence", 5, s4.size());
    	
    	
    }
    
    private static void testEnsureCapacity() {
    	Testing.testSection("Testing ensureCapacity");
    	
    	Sequence s1 = new Sequence();
    	s1.ensureCapacity(5);
    	Testing.assertEquals("ensure to less than current is still current",
    			10, s1.getCapacity());
    	
    	s1.ensureCapacity(10);
    	Testing.assertEquals("Ensure to current keeps current", 10, s1.getCapacity());
    	
    	s1.ensureCapacity(15);
    	Testing.assertEquals("ensure to more than current", 15, s1.getCapacity());
    	
    	Sequence s2 = new Sequence(0);
    	s2.ensureCapacity(-1);
    	Testing.assertEquals("Doesnt crash on negative ensure", 0, s2.getCapacity());
    	
    	s2.ensureCapacity(5);
    	Testing.assertEquals("ensure works on empty capacity", 5, s2.getCapacity());
    }
    
    private static void testGetCurrent() {
    	Testing.testSection("Testing getCurrent");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("Default constructor with no items", null, s1.getCurrent());
    	
    	s1.addAfter("a");
    	Testing.assertEquals("Single Item", "a", s1.getCurrent());
    	
    	s1.addAfter("b");
    	Testing.assertEquals("Secondary item", "b", s1.getCurrent());
    	
    	s1.advance();
    	Testing.assertEquals("At end of sequence", null, s1.getCurrent());
    	
    	s1.start();
    	Testing.assertEquals("Back to first item", "a", s1.getCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("No capacity sequence", null, s2.getCurrent());
    }
    
    private static void testGetCapacity() {
    	Testing.testSection("Testing getCapacity");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("Default constructor capacity", 10, s1.getCapacity());
    	
    	Sequence s2 = new Sequence(5);
    	Testing.assertEquals("Non default constructor capacity", 5, s2.getCapacity());
    	
    	Sequence s3 = new Sequence(0);
    	Testing.assertEquals("No capacity constructor capacity", 0, s3.getCapacity());
    }
    
    private static void testIsCurrent() {
    	Testing.testSection("Testing isCurrent");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("isCurrent on unused sequence", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	Testing.assertEquals("isCurrent on current item", true, s1.isCurrent());
    	
    	s1.advance();
    	Testing.assertEquals("isCurrent on advance past last item", false, s1.isCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("isCurrent on no capacity", false, s2.isCurrent());
    }
    
    private static void testAddAfter() {
    	Testing.testSection("Testing addAfter");
    	
    	Sequence s1 = new Sequence();
    	s1.addAfter("a");
    	Testing.assertEquals("Add single item to empty", "{>a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after single add", 1, s1.size());
    	
    	s1.addAfter("b");
    	Testing.assertEquals("Add to sequence already existing", "{a, >b} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after add", 2, s1.size());
    	
    	Sequence s2 = new Sequence(0);
    	s2.addAfter("a");
    	Testing.assertEquals("Add to no capacity", "{>a} (capacity = 1)", s2.toString());
    	
    	s2.addAfter("b");
    	Testing.assertEquals("Add to filled capacity", "{a, >b} (capacity = 3)", s2.toString());
    	
    	s2.advance();
    	s2.addAfter("c");
    	Testing.assertEquals("Add when past end", "{a, b, >c} (capacity = 3)", s2.toString());
    }
    
    private static void testAddBefore() {
    	Testing.testSection("Testing addBefore");
    	
    	Sequence s1 = new Sequence();
    	s1.addBefore("a");
    	Testing.assertEquals("Add single item to empty", "{>a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after single add", 1, s1.size());
    	
    	s1.addBefore("b");
    	Testing.assertEquals("Add to sequence already existing", "{>b, a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after add", 2, s1.size());
    	
    	s1.advance();
    	s1.advance();
    	s1.addBefore("c");
    	Testing.assertEquals("Add with no current sets at beginning", "{>c, b, a} (capacity = 10)", s1.toString());
    	
    	Sequence s2 = new Sequence(0);
    	s2.addBefore("a");
    	Testing.assertEquals("Add to no capacity", "{>a} (capacity = 1)", s2.toString());
    	
    	s2.addBefore("b");
    	Testing.assertEquals("Add to filled capacity", "{>b, a} (capacity = 3)", s2.toString());
    }
    
	private static void testCreate()
	{
		Testing.testSection("Creation tests and toString of empty sequence");
		
		Sequence s1 = new Sequence();
		Testing.assertEquals("Default constructor", "{} (capacity = 10)", s1.toString());
		Testing.assertEquals("Default constructor, initial size", 0, s1.size());
		
		Sequence s2 = new Sequence(20);
		Testing.assertEquals("Non-default constructor", "{} (capacity = 20)", s2.toString());
		Testing.assertEquals("Non-default constructor, initial size", 0, s2.size());
	}
}
