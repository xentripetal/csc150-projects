/*I affirm that I have carried out the attached academic endeavors with full academic honesty,
 * in accordance with the Union College Honor Code and the course syllabus.
 * Corbin Martin
 * CSC-150-01
 * 1/23/2017
 */
import solitaire.SolitaireSimulator;;

/**
 * Runs a solitaire simulation.
 * @author Cyrusc
 *
 */
public class Client {

	/**
	 * Runs a simulation of simplified Solitaire and calculates wins.
	 * @param args unusued
	 */
	public static void main(String[] args) {
		SolitaireSimulator sim = new SolitaireSimulator();
		int matchesWon;
		
		for (int multiplier = 1; multiplier <= 10; multiplier++) {
			matchesWon = 0;

			//Runs the experiment 1000 * multiplier times and checks if 
			// won based on boolean output of playGame method.
			for (int i = 1; i <= 1000 * multiplier; i++) 
				if (sim.playGame())
					 matchesWon++;
			
			float percentWin = (float) matchesWon / (float) (multiplier * 1000);
			System.out.println(matchesWon + "/" + (multiplier * 1000) +
					" games won = " + percentWin + "%");
		}
	}

}
