package solitaire;
/**
 * Manages and creates a Simplified Solitaire simulator
 * based on Deck and Card objects.
 * @author Corbin Martin
 *
 */
public class SolitaireSimulator {
	private Deck myDeck;
	private Card[] activeCards;
	//You criticized me for using a size variable in project 1 
	//but request it be used in project 2
	private int cardsInPlay;
	
	
	/**
	 * 
	 */
	public SolitaireSimulator() {
		myDeck = new Deck();
		activeCards = new Card[0];
		cardsInPlay = 0;
	}
	
	/**
	 * Empties active cards. Does not reset the internal deck
	 */
	private void reset() {
		activeCards = new Card[0];
		cardsInPlay = 0;
		
	}
	
	/**
	 * Runs a simulated game of solitaire. 
	 * @return Boolean for if simulated game was won. True means won
	 * and False means loss.
	 */
	public boolean playGame() {
		if (cardsInPlay != 0 || activeCards.length != 0)
			reset();
		
		//shuffle deck before every game
		myDeck.shuffle();
		
		return gameLoop();
	}
	
	/**
	 * Main logic loop for game. Handles begin and end of game and
	 * what methods to call for game rules. 
	 * @return Boolean for if simulated game was won. True means won
	 * and False means loss.
	 */
	private boolean gameLoop() {
		boolean keepRemoving = true;
		boolean activeGame = true;
		
		while (activeGame) { 
			keepRemoving = true;
			
			//Always draw one card on loop as atleast one card must always be
			//drawn if no more cards can be removed
			activeGame = appendCard();
			
			//Keep adding cards untill atleast 4 our present or deck runs out
			while (cardsInPlay < 4 && activeGame)
				activeGame = appendCard();
			
			//Remove cards as long as there are at least 4 cards or no more 
			//cards can be removed
			while (keepRemoving && cardsInPlay >= 4)
				keepRemoving = checkAndRemoveCards();
		}
		
		//If no cards left in deck then game was won. No cleanup as cleanup is
		//done on start
		if (cardsInPlay == 0)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks right-most 4 cards to see if all four match suit or the
	 * outer 2 match suit and removing all inner 2, respectively.
	 * @return Boolean corresponding to if any cards were removed or not
	 */
	private boolean checkAndRemoveCards() {
		boolean fourFound = true;
		boolean twoFound = true;
		
		//relative position of rightmost 4 cards in activeCards.
		int transform = cardsInPlay - 4;
		//suit of the first position in the rightmost 4 cards.
		String cachedSuit = activeCards[transform].getSuit();
		
		//iterate over 3 remaining cards and check if they match suits
		for (int i = transform + 1; i < cardsInPlay; i++) {
			if (!activeCards[i].getSuit().equals(cachedSuit)) {
				//if right most card is not same suit.
				if (i == transform + 3)
					twoFound = false;
				fourFound = false;
			}
		}
		
		if (fourFound) {
			//remove right most 4
			int[] indices = {transform, transform + 1, 
					transform + 2, transform + 3};
 			popCards(indices);
		}
		else if (twoFound) {
			//remove middle 2
			int[] indices = {transform + 1, transform + 2};
			popCards(indices);
		}
		
		return (fourFound || twoFound);
	}
	
	/**
	 * Remove cards from the collection of active cards.
	 * @param indices collection of int's containting index's of cards to remove
	 */
	private void popCards(int[] indices) {
		//update cardsInPlay to new length
		cardsInPlay = cardsInPlay - indices.length;
		Card[] tempDeck = new Card[cardsInPlay];
		boolean isMatch;
		int tempIndex = 0;
		
		//copy current active cards to new temp one unless its at a position
		//given by indices.
		for (int i = 0; i < activeCards.length; i++) {
			isMatch = false;
			
			for (int x = 0; x < indices.length && !isMatch; x++) 
				if (indices[x] == i)
					isMatch = true;
			
			if (!isMatch) {
				tempDeck[tempIndex] = activeCards[i];
				tempIndex++;
			}
		}
		
		activeCards = tempDeck;
	}
	
	/**
	 * Deals next card from deck and puts it into the active cards.
	 * @return Boolean for if a new card could be added. 
	 */
	private boolean appendCard() {
		if (myDeck.atEnd())
			return false;
		
		Card card = myDeck.deal();
		
		cardsInPlay++;
		Card[] tempDeck = new Card[cardsInPlay];
		
		//copy all old cards into tempDeck
		for (int i = 0; i<activeCards.length; i++)
			tempDeck[i] = activeCards[i];
		
		//put new card in the last position in deck.
		tempDeck[cardsInPlay - 1] = card;
		activeCards = tempDeck;
		
		return true;
	}
}
