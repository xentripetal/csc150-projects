package solitaire;
import java.util.Random;
/**
 * Manages and creates a standard deck of 52 cards.
 * @author Corbin Martin
 *
 */
public class Deck {
	private final static String[] values = {"Ace", "Two", "Three",
			"Four", "Five", "Six", "Seven", "Eight", 
			"Nine", "Ten", "Jack", "Queen", "King"};
	private final static String[] suits = {"Club", "Spade", "Heart", "Diamond"};
	
	private Card[] cards;
	private int topCard;
	
	/**
	 * Constructs deck of 52 cards and populates in standard form.
	 */
	public Deck() {
		cards = new Card[52];
		topCard = 0;
		populateDeck();
	}
	
	/**
	 * populates deck based on values and suits collections.
	 */
	private void populateDeck() {
		int i = 0;
		for (String Value : values) {
			for (String Suit : suits) {
				cards[i] = new Card(Value, Suit);
				i++;
			}
		}
	}
	
	/**
	 * returns human readable String of all cards in deck in order.
	 */
	public String toString() {
		String concatString = "";
		
		for (Card card : cards)
			concatString += card.getValue() + " of " + card.getSuit() + "s\n";
		
		return concatString;
			
	}
	
	/**
	 * Check if at end of deck.
	 * @return True if at end, false if not at end.
	 */
	public boolean atEnd() {
		if (topCard >= cards.length)
			return true;
		
		return false;
	}
	
	/**
	 * Gets next card in deck
	 * @return Card of current top card in deck.
	 */
	public Card deal() {
		Card card = cards[topCard];
		topCard++;
		return card;
	}
	
	/**
	 * Shuffles the deck by random crossing for every position.
	 */
	public void shuffle() {
		Random prng = new Random();
		int rand;
		for (int i = 0; i < cards.length; i++) {
			Card holder = cards[i];
			rand = prng.nextInt(52);
			cards[i] = cards[rand];
			cards[rand] = holder;
		}
		//reset topCard as context is loss in a shuffle.
		topCard = 0;
	}
	
	

}
