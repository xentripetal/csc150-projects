package crazyeight;

/**
 * Manages and creates a single card.
 * 
 * @author Corbin Martin
 *
 */
public class Card {

	public final static String[] validValues = { "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
			"Ten", "Jack", "Queen", "King" };
	public final static String[] validSuits = { "Club", "Spade", "Heart", "Diamond" };

	private String value;
	private String suit;

	/**
	 * constructs a card given a Suit and Value. Validates input.
	 * 
	 * @param Value
	 *            value of card. case expected: "Ace","Two",..."King"
	 * @param Suit
	 *            suit of card. case expected: "Club","Diamond",...
	 */
	public Card(String Value, String Suit) {
		checkValidInput(Value, Suit);
		value = Value;
		suit = Suit;
	}

	/**
	 * Checks input of value and suit. Throws IllegalArgumentException if
	 * invalid
	 * 
	 * @param Value
	 *            value of card. case expected: "Ace","Two",..."King"
	 * @param Suit
	 *            suit of card. case expected: "Club","Diamond",...
	 */
	private void checkValidInput(String Value, String Suit) {
		if (!isValidSuit(Suit))
			throw new IllegalArgumentException("Suit '" + Suit + "' is not valid.");

		if (!isValidValue(Value))
			throw new IllegalArgumentException("Value '" + Value + "' is not valid.");
	}

	/**
	 * Checks if Value exists in collection of valid Values.
	 * 
	 * @param Value
	 *            value of card. case expected: "Ace","Two",..."King"
	 * @return True if valid, False if invalid.
	 */
	private boolean isValidValue(String Value) {
		boolean isValueValid = false;

		for (int i = 0; i < validValues.length && !isValueValid; i++)
			if (validValues[i].equals(Value))
				isValueValid = true;

		return isValueValid;
	}

	/**
	 * Checks if Suit exists in collection of valid Suits.
	 * 
	 * @param Suit suit of card. case expected: "Club","Diamond",...
	 * @return True if valid, False if invalid.
	 */
	private boolean isValidSuit(String Suit) {
		boolean isSuitValid = false;

		for (int i = 0; i < validSuits.length && !isSuitValid; i++)
			if (validSuits[i].equals(Suit))
				isSuitValid = true;

		return isSuitValid;
	}

	/**
	 * Returns human readable Value and Suit of card.
	 */
	public String toString() {
		return value + " of " + suit + "s";
	}

	/**
	 * Gets Value of Card
	 * 
	 * @return card Value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets Suit of Card
	 * 
	 * @return card Suit
	 */
	public String getSuit() {
		return suit;
	}
}
