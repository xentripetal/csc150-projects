package crazyeight;

/**
 * Creates and handles a players Hand of cards
 * @author Corbin Martin
 */
public class Hand {
	private final static int DEFAULT_HAND_SIZE = 7;

	private String player;
	private Card[] cards;
	private Deck deck;

	/**
	 * Constructs the players hand and fills with 7 cards
	 * @param player Players Name
	 * @param deck Source deck to draw from
	 */
	public Hand(String player, Deck deck) {
		this.player = player;
		this.deck = deck;
		cards = new Card[DEFAULT_HAND_SIZE];

		for (int i = 0; i < cards.length; i++)
			cards[i] = deck.deal();
	}

	/**
	 * Draws a card from the deck and adds to hand
	 */
	public void drawCard() {
		if (!deck.atEnd()) {
			Card[] tempDeck = new Card[cards.length + 1];
			for (int i = 0; i < cards.length; i++)
				tempDeck[i] = cards[i];

			tempDeck[cards.length] = deck.deal();
			cards = tempDeck;
		} else {
			System.out.println("There are no more cards in the deck");
		}
	}

	/**
	 * Gets the current hand size 
	 * @return Integer of how many cards in hand
	 */
	public int getSize() {
		return cards.length;
	}

	/**
	 * Removes a card by a given relative index
	 * @param index Position of card to remove in players hand
	 * @return The card that was removed
	 */
	public Card RemoveCard(int index) {
		if (cards.length == 0)
			return null;

		Card[] tempDeck = new Card[cards.length - 1];
		Card holder = cards[index];
		int tempIndex = 0;

		for (int i = 0; i < cards.length; i++) {
			if (!(i == index)) {
				tempDeck[tempIndex] = cards[i];
				tempIndex++;
			}
		}

		cards = tempDeck;
		return holder;
	}

	/**
	 * Return the players name
	 * @return Players name
	 */
	public String getPlayerName() {
		return player;
	}

	/**
	 * Gets card at given index
	 * @param index relative index of card to remove
	 * @return The card at given index
	 */
	public Card seekCard(int index) {
		if (index < 0 || index >= getSize())
			return null;
		
		return cards[index];
	}

	/**
	 * {@inheritDoc}
	 * Returns a string of object containing a formatted player
	 * name and the cards in their hand with their relative positions.
	 * @see Object#toString()
	 */
	public String toString() {
		String concatString = player + "'s Hand\n" + "-------------------\n";
		for (int i = 0; i < cards.length; i++)
			concatString += i + ") " + cards[i].toString() + "\n";

		return concatString;
	}

}
