package crazyeight;

import java.util.Scanner;

/**
 * Creates and handles a game of CrazyEights
 * @author Corbin Martin
 */
public class CrazyEights {

	private Card topCard;
	private Hand[] players;
	private Deck deck;
	private Card crazyEight;

	
	/**
	 * Creates the CrazyEight and takes input for players
	 * and generates players hands.
	 */
	public CrazyEights() {
		crazyEight = null;
		topCard = null;
		deck = new Deck(104);
		System.out.println("How many players: ");
		players = new Hand[getIntInRange(2, 13)];
		deck.shuffle();

		//Get player names and generate Hands with cards
		for (int i = 0; i < players.length; i++) {
			System.out.println("Player " + (i + 1) + " name: ");
			String playername = inputPlayerName();
			players[i] = new Hand(playername, deck);
		}
	}

	/**
	 * Takes input for player name and returns name
	 * @return
	 */
	private String inputPlayerName() {
		Scanner scanner = new Scanner(System.in);
		return scanner.nextLine();
	}

	/**
	 * Takes input for a integer in the defined range.
	 * Inclusive.
	 * @param min Minimum integer to accept
	 * @param max Maximum integer to accept
	 * @return The users valid input
	 */
	private int getIntInRange(int min, int max) {
		Scanner scanner = new Scanner(System.in);
		int input = -1;
		boolean firstInput = true;
		
		while (input < min || input > max) {
			if (!firstInput)
				System.out.println("Please enter a number between " + min + " and " + max);

			while (!scanner.hasNextInt()) {
				scanner.next();
				System.out.println("Please enter a number between " + min + " and " + max);
			}
			
			input = scanner.nextInt();
			firstInput = false;
		}

		return input;
	}

	/**
	 * Runs a game of Crazy Eight. assumes players are
	 * Initialized and the deck is shuffled and present. 
	 */
	public void playGame() {
		boolean isGameOver = false;

		while (!isGameOver) {
			for (int i = 0; i < players.length && !isGameOver; i++) {
				Hand player = players[i];
				isGameOver = playerTurn(player);
			}
		}
	}

	/**
	 * Handles turn for player in CrazyEight. 
	 * @param player The player to handle for turn
	 * @return True if player has no cards left, False if not.
	 */
	private boolean playerTurn(Hand player) {
		boolean isTurnOver = false;
		
		System.out.println(player.getPlayerName() + " Turn\n");

		while (!isTurnOver) {
			//take player input and check the input and handle it 
			//until a valid turn ending move is played
			int move = getMove(player);
			isTurnOver = handleCardCheck(player, move);
		}

		//one card left warning
		if (player.getSize() == 1)
			System.out.println(player.getPlayerName() + " only has 1 card left!");

		//player has won the game
		if (player.getSize() == 0) {
			System.out.println(player.getPlayerName() + " has won the game!");
			return true;
		}

		return false;
	}
	
	/**
	 * Prints information for player and takes input for player move
	 * @param player Player to check move for
	 * @return Returns players selection of move
	 */
	private int getMove(Hand player) {
		int maxValue = player.getSize();

		//Print players hand
		System.out.println(player.toString());

		//Pass or Draw option
		if (topCard != null && !deck.atEnd()) {
			System.out.println(player.getSize() + ") Draw a new card");
		} else if (deck.atEnd()) {
			System.out.println(player.getSize() + ") Pass");
		}

		//print the top card
		if (topCard != null) {
			if (crazyEight == null) {
				System.out.println("\nTop card is " + topCard.toString());
			} else {
				System.out.println("\nTop card is " + crazyEight.toString());
				System.out.println("(but is acting as a " + topCard.getSuit() + ")");
			}
		} else {
			//dont include a additional option if its the first move of the game
			maxValue--;
		}
		System.out.println("Move: ");

		return getIntInRange(0, maxValue);
	}

	/**
	 * Checks players input and carries out the specified action if valid.
	 * @param player Player to handle move for
	 * @param move Move the player specified
	 * @return True if turn ending move, false if not.
	 */
	private boolean handleCardCheck(Hand player, int move) {
		//Additional option move (Draw or Pass)
		if (move == player.getSize()) {
			if (deck.atEnd()) {
				return true;
			} else {
				player.drawCard();
				return false;
			}
		} else {
			//If first move place card and move on
			if (topCard == null) {
				setTopCard(player.RemoveCard(move));
				return true;
			}
			// else check card to see if its valid
			Card drawnCard = player.seekCard(move);
			if (drawnCard != null) {
				return checkValidCard(drawnCard, player, move);
			} else {
				System.out.println("That card does not exist");
			}
		}

		//Any valid scenario returns true. so if here a invalid move was played
		return false;
	}
	
	/**
	 * Checks and replaces topCard or Crazyeight 
	 * @param drawnCard The card that is being checked
	 * @param player Player who has drawn the card
	 * @param move move the player made. must match drawnCard
	 * @return True if a valid card, false if not.
	 */
	private boolean checkValidCard(Card drawnCard, Hand player, int move) {
		if (drawnCard.getValue().equals("Eight")) {
			//Crazy Eight card
			crazyEight = player.RemoveCard(move);
			topCard = crazyEightEvent();
			return true;
		} else if (drawnCard.getSuit().equals(topCard.getSuit())) {
			//Valid Suit card
			setTopCard(player.RemoveCard(move));
			return true;
		} else if (drawnCard.getValue().equals(topCard.getValue())) {
			//Valid Value Card
			setTopCard(player.RemoveCard(move));
			return true;
		} else {
			System.out.println("That is not a valid card. Try again");
		}
		return false;
	}
	
	/**
	 * Sets the top card and resets the crazyEight card if it exists
	 * @param card
	 */
	private void setTopCard(Card card) {
		if (crazyEight != null)
			crazyEight = null;
		topCard = card;
	}

	/**
	 * takes input for what suit a crazy eight will play as
	 * Does not remove the played card.
	 * @return Crazy Eight card the player inputed.
	 */
	private Card crazyEightEvent() {
		Scanner scanner = new Scanner(System.in);
		boolean validInput = false;
		Card eightCard = null;
		
		System.out.println(" #### Crazy Eight #### ");
		while (!validInput) {
			System.out.println("Which Suit? (C, D, H, S)");
			String input = scanner.nextLine();
			validInput = true;
			switch (input) {
			case "C":
				eightCard = new Card("Eight", "Club");
				break;
			case "D":
				eightCard = new Card("Eight", "Diamond");
				break;
			case "H":
				eightCard = new Card("Eight", "Heart");
				break;
			case "S":
				eightCard = new Card("Eight", "Spade");
				break;
			default:
				validInput = false;
				break;
			}
		}
		
		return eightCard;
	}

}
