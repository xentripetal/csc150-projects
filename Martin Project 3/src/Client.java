
import crazyeight.*;
/**
 * Hooks into crazyeight game and runs the game
 * @author Corbin Martin
 */
public class Client {

	/**
	 * Creates and runs a game of CrazyEights
	 * @param args ignored
	 */
	public static void main(String[] args) {

		CrazyEights game = new CrazyEights();
		game.playGame();
	}

}
