package Tokens;
import Utils.Stack;

/** Describes the methods that must be defined in order for an
 * object to be considered a token.  Every token must be able
 * to be processed (handle), printable (toString), and have some priority.
 * 
 * @author Chris Fernandes & Corbin Martin
 * @version 10/26/08
 *
 */
public interface Token
{	
	/** Processes the current token.  Since every token will handle
	 *  itself in its own way, handling may involve pushing or
	 *  popping from the given stack and/or appending more tokens
	 *  to the output string.
	 * 
	 *  @param s the Stack the token uses, if necessary, when processing itself.
	 *  @return String to be appended to the output
	 */
    public String handle(Stack<Token> s);
    
    /** Returns the token as a printable String
     * 
     *  @return the String version of the token.  For example, ")"
     *  for a right parenthesis.
     */
    public String toString();
    
    /** 
     * Note that higher int is higher priority
     * @return The int priority of method. 
     */
    public int getPriority();

}
