package Tokens;

import Utils.Stack;

/**
 * A Plus operator of Token type
 * @author Corbin Martin
 */
public class LeftParen implements Token
{
	private int priority = 4;
	/**
	 * Returns the operators symbol
	 */
    public String toString() {
    	return "(";
    }
    
    /**
     * Handles the implementation of the operator to stack.
     * @param s Stack to append operations too.
     * @return String to be appended to the output
     */
    public String handle(Stack<Token> s)
    {
        s.push(this);
        return "";
    }
    
    /**
     * Getter for Priority
     */
    public int getPriority() {
    	return priority;
    }
}
