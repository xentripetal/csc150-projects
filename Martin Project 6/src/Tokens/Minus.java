package Tokens;

import Utils.Stack;

/**
 * A Plus operator of Token type
 * @author Corbin Martin
 */
public class Minus implements Token
{
	private int priority = 1;
	/**
	 * Returns the operators symbol
	 */
    public String toString() {
    	return "-";
    }
    
    /**
     * Handles the implementation of the operator to stack.
     * @param s Stack to append operations too.
     * @return String to be appended to the output
     */
    public String handle(Stack<Token> s)
    {
    	String postfix = "";
        while (!s.isEmpty() && !(s.peek() instanceof LeftParen) && s.peek().getPriority() > getPriority()) {
        	postfix += s.pop();
        }
        s.push(this);
        return postfix;
    }
    
    /**
     * Getter for Priority
     */
    public int getPriority() {
    	return priority;
    }
}
