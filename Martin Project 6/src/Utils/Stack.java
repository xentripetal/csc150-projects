package Utils;

/**
 * Creates and handles a stack ADT
 * @author Corbin Martin
 * @param <T> Data Type of Stack
 * 
 * Invariants: 
 * 
 * 1. Data is never null.
 * 2. Data inputs and outputs are always of type <T>
 * 3. Inherits Invariants from LinkedList
 */
public class Stack<T>
{
	
	private LinkedList data;
    
	/**
	 * Stack Constructor
	 */
    public Stack() {
    	data = new LinkedList();
    }
   
    /**
     * Checks if the Stack is empty
     * @return true if empty, false if not
     */
    public boolean isEmpty() {  
    	return data.isEmpty();
    }

    /**
     * Inserts toPush into top of stack
     * @param toPush Object of type <T> into top of stack
     */
    public void push(T toPush) {
    	data.insertAtHead(toPush);
    }
    
    /**
     * Removes and returns the object at top of stack
     * Throws IndexOutOfBoundsException if empty
     * @return Object at top of stack
     */
    @SuppressWarnings("unchecked")
	public T pop()
    {
    	return (T) data.removeHead();
    } 
  
    /**
     * Returns the object at the top of stack
     * Throws IndexOutOfBoundsException if empty
     * @return object at top
     */
    @SuppressWarnings("unchecked")
    public T peek() 
    {
    	return (T) data.peek(0);
    } 
    
    /**
     * Returns size
     * @return Size of the Stack
     */
    public int size() {
    	return data.getLength();
    }
     
    /**
     * Returns a human readable string of the stack
     */
    public String toString() {
        String concatString = "{>";
    	for (int i = 0; i < size(); i++) {
        	concatString += data.peek(i);
        	if (i < size() - 1)
        		concatString += ", ";
        }
    	concatString += "}";
    	return concatString;
    }
    
} 
   

