package Utils;


/**
 * Creates and handles a generic ListNode object
 * @author Corbin Martin
 *
 */
public class ListNode {
	public Object content;
	public ListNode next;
	
	/**
	 * Creates a ListNode with the defined content
	 * @param content Object for ListNode to hold
	 */
	public ListNode(Object content) {
		this.content = content;
	}
	
	/**
	 * Returns a string of content info
	 */
	public String toString() {
		return "" + content;
	}

}
