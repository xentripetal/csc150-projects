package Utils;

import Tokens.*;
/**
 * Runs a conversion of a infix string to postfix
 * @author Corbin Martin
 */
public class Converter {
	
	private final String DEFAULT_FILE_PATH = "src/input.txt";
	/**
	 * Runs conversion of the defined input file.
	 * Writes to system.out
	 */
	public void convert() {
		FileReader reader = new FileReader(DEFAULT_FILE_PATH);
		runThroughTokens(reader);
	}
	
	private void runThroughTokens(FileReader reader) {
		String postFix = "";
		
		
		while (postFix != "EOF") {
			postFix = runThroughLine(reader);
			System.out.println(postFix);
		}
	}
	
	private String runThroughLine(FileReader reader) {
		Stack<Token> tokens = new Stack<Token>();
		String postfix = "";
		String rawToken = "";
		Token token = null;
		
		while (!(token instanceof EOL)) {
			rawToken = reader.nextToken();
			
			// If end of file break out and stop. 
			// There should never be a end of file before EOL
			if (rawToken.equals("EOF")) {
				return "EOF";
			}
			
			token = getToken(rawToken);
			
			//is null only if no Token exists, therefore is operand
			if (token == null) {
				postfix += rawToken;
			} else {
				postfix += token.handle(tokens);
			}
		}
		
		return postfix;
	}
	
	private static Token getToken(String token) {
		if (token.equals("+"))
			return new Plus();
		else if (token.equals("-"))
			return new Minus();
		else if (token.equals("*"))
			return new Multiply();
		else if (token.equals("/"))
			return new Divide();
		else if (token.equals("("))
			return new LeftParen();
		else if (token.equals(")"))
			return new RightParen();
		else if (token.equals("^"))
			return new Exponent();
		else if (token.equals(";"))
			return new EOL();
		else 
			return null;
		
	}

}
