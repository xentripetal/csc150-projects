import Utils.Converter;

/**
 * I hereby state that all attached documents adhere to the Union College Honor Code.
 * Corbin Martin
 * Runs and activates Converter
 * as well as tests
 * @author Corbin Martin
 */
public class Client {

	public static void main(String[] args) {
		runTests(true);
		
		Converter converter = new Converter();
		converter.convert();
	}
	
	private static void runTests(boolean verbose) {
		Tests.Testing.setVerbose(verbose);
		Tests.Testing.startTests();
		
		Tests.ListTester.runTests();
		Tests.StackTester.runTests();

		Tests.Testing.finishTests();
		
	}

}
