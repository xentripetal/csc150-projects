package Tests;
import Utils.LinkedList;
import Utils.ListNode;

/** Tester for the Event and LinkedList classes
 * 
 * @author <em>Corbin Martin</em>, Matt Anderson, Aaron Cass, and Chris Fernandes
 * @version 2/08/17
 */
public class ListTester {    
    /**
     * Runs a series of tests on LinkedList and Events
     * @param args
     */
    public static void runTests()
    {
    	testConstructor();
    	testListNode();
    	testToString();
    	testRemoveHead();
    	testInsertAtHead();
    	testInsertAtTail();
    	testInsert();
    	testPeek();
    	testGetLastIndex();
    	testGetFirstIndex();
    	testRemove();
    	
    }
    
    private static void testConstructor()
    {
    	Testing.testSection("LinkedList: Constructor test");
	
    	LinkedList list1 = new LinkedList();
    	Testing.assertEquals("Default constructor", "()", list1.toString());
    }
    
    private static void testInsertAtHead() {
    	Testing.testSection("LinkedList: insertAtHead");
    	LinkedList list = new LinkedList();
    	list.insertAtHead("a");
    	Testing.assertEquals("Insert with empty", "(a)", list.toString());
    	
    	list.insertAtHead("b");
    	Testing.assertEquals("Insert with others", "(b,\na)", list.toString());
    	
    	list.insertAtHead(null);
    	Testing.assertEquals("Insert null", "(null,\nb,\na)", list.toString());
    }
    
    private static void testToString() {
    	Testing.testSection("LinkedList: toString");
    	
    	LinkedList list1 = new LinkedList();
    	Testing.assertEquals("Default Constructor", "()", list1.toString());
    	
    	list1.insertAtHead("a");
    	Testing.assertEquals("Singleton toString", "(a)", list1.toString());
    	
    	list1.insertAtHead("b");
    	list1.insertAtHead("c");
    	Testing.assertEquals("Normal toString", "(c,\nb,\na)", list1.toString());
    	
    	list1.insertAtTail(null);
    	Testing.assertEquals("Null toString", "(c,\nb,\na,\nnull)", list1.toString());
    	
    }
    
    private static void testRemove() {
			Testing.testSection("LinkedList: remove test");
			
	    	LinkedList list1 = new LinkedList();
	    	boolean exception = false;
	    	try {
	    		list1.remove(0);
	    	} catch (IndexOutOfBoundsException e) {
	    		exception = true;
	    	}
	    	Testing.assertEquals("Remove on empty is error", true, 
	    			exception);
	    	
	    	list1.insertAtHead("a");
	    	Testing.assertEquals("Remove on singleton", "a", list1.remove(0));
	    	Testing.assertEquals("Size is updated", 0, list1.getLength());
	    	
	    	list1.insertAtTail("a");
	    	list1.insertAtTail("b");
	    	Testing.assertEquals("Remove with next", "a", list1.remove(0));
	    	Testing.assertEquals("Size is updated", 1, list1.getLength());
	    	
	    	list1.insertAtHead("a");
	    	list1.insertAtTail("c");
	    	Testing.assertEquals("Remove with previous and next", "b", list1.remove(1));
	    	
	    	Testing.assertEquals("Remove with no next", "c", list1.remove(1));
	    	
    }
    
    private static void testInsert() {
		Testing.testSection("LinkedList: insert test");
    	LinkedList list1 = new LinkedList();
    	list1.insert(0, "a");
    	Testing.assertEquals("Insert on empty", "(a)", list1.toString());
    	Testing.assertEquals("Size is updated on empty", 1, list1.getLength());

    	list1.insert(1, "b");
    	Testing.assertEquals("Insert on non empty", "(a,\nb)", list1.toString());
    	Testing.assertEquals("Size is updated on non empty", 2, list1.getLength());

    	boolean exception = false;
    	try {
    		list1.insert(5, "c");
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("Insert past size is error", true, 
    			exception);
    	
    	 exception = false;
    	try {
    		list1.insert(-1, "c");
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("Insert before 0 is error", true, 
    			exception);
    	
    	list1.insert(0, "c");
    	Testing.assertEquals("Insert at 0 is head", "(c,\na,\nb)", list1.toString());
    	Testing.assertEquals("Size is updated on multiple", 3, list1.getLength());
    	
    	
    }
     
    
    private static void testListNode() {
			Testing.testSection("ListNode: Test");
    	ListNode testNode = new ListNode(null);
    	Testing.assertEquals("ListNode nullpointer check", "null", testNode.toString());
    }
    
    private static void testRemoveHead() {
    	Testing.testSection("LinkedList: removeHead test");
    	
    	String sampleEvent1 = "book club";
    	String sampleEvent2 = "chess club";
    	LinkedList list1 = new LinkedList();
    	
    	boolean exception = false;
    	try {
    		list1.removeHead();
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("removeHead of empty", true, 
    			exception);
    	
    	list1.insertAtHead(sampleEvent1);
    	Testing.assertEquals("removeHead of singleton", sampleEvent1, 
    			list1.removeHead());
    	Testing.assertEquals("removeHead of singleton empties list", 0, 
    			list1.getLength());
    	
    	list1.insertAtHead(sampleEvent1);
    	list1.insertAtHead(sampleEvent2);
    	Testing.assertEquals("removeHead of multiple", sampleEvent2, 
    			list1.removeHead());
    	
    	Testing.assertEquals("removeHead sets next link", sampleEvent1, 
    			list1.removeHead());

    }
    
    private static void testInsertAtTail() {
    	Testing.testSection("LinkedList: insertAtTail test");
    	
    	String sampleEvent1 = "book club";
    	String sampleEvent2 = "chess club";
    	LinkedList list1 = new LinkedList();
    	list1.insertAtTail(sampleEvent1);
    	
    	Testing.assertEquals("insert on empty is head", sampleEvent1, 
    			list1.removeHead());
    	
    	list1.insertAtTail(sampleEvent1);
    	list1.insertAtTail(sampleEvent2);
    	Testing.assertEquals("insert on non empty is tail", 
    			"(" + sampleEvent1 + ",\n" + sampleEvent2 + ")",
    			list1.toString());
    	
    	list1.insertAtTail(sampleEvent1);
    	Testing.assertEquals("insert repetetive is tail", 
    			"(" + sampleEvent1 + ",\n" + sampleEvent2 + ",\n" 
    				+ sampleEvent1 + ")",
    			list1.toString());
    }
    
    private static void testGetLastIndex() {
    	Testing.testSection("LinkedList: search test");
    	String sampleEvent1 = "book club";
    	String sampleEvent2 = "chess club";
    	LinkedList list1 = new LinkedList();
    	Testing.assertEquals("search on empty", -1, list1.getLastIndex("book club"));
    	
    	list1.insertAtHead(sampleEvent1);
    	Testing.assertEquals("search on singleton", 0, list1.getLastIndex("book club"));
    	
    	list1.insertAtHead(sampleEvent2);
    	Testing.assertEquals("search on multiple in non first", 1, list1.getLastIndex("book club"));
    	
    	list1.removeHead();
    	list1.insertAtTail(sampleEvent2);
    	Testing.assertEquals("search on multiple in first", 0, list1.getLastIndex("book club"));

    	list1.insertAtHead(sampleEvent1);
    	Testing.assertEquals("search on duplicates", 1, list1.getLastIndex("book club"));
    	
    	Testing.assertEquals("works for all inputs", 2, list1.getLastIndex("chess club"));
     	
    }
    
    private static void testPeek() {
    	Testing.testSection("LinkedList: Peek");
    	
    	String sample1 = "a";
    	String sample2 = "b";
    	String sample3 = "c";
    	
    	LinkedList list1 = new LinkedList();
    	
    	boolean exception = false;
    	try {
    		list1.peek(0);
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	
    	Testing.assertEquals("Peek on empty is exception", true, exception);
    	
    	list1.insertAtHead(sample1);
    	Testing.assertEquals("Peek on singleton", "a", list1.peek(0));
    	
    	list1.insertAtHead(sample2);
    	list1.insertAtHead(sample3);
    	Testing.assertEquals("Peek on multiple first", "c", list1.peek(0));
    	Testing.assertEquals("Peek on multiple nonfirst", "a", list1.peek(2));
    	
    	
    	exception = false;
    	try {
    		list1.peek(100);
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("Peek outside of range is exception", true, exception);
    }
    
    private static void testGetFirstIndex() {
    	Testing.testSection("LinkedList: search test");
    	String sampleEvent1 = "book club";
    	String sampleEvent2 = "chess club";
    	LinkedList list1 = new LinkedList();
    	Testing.assertEquals("search on empty", -1, list1.getFirstIndex("book club"));
    	
    	list1.insertAtHead(sampleEvent1);
    	Testing.assertEquals("search on singleton", 0, list1.getFirstIndex("book club"));
    	
    	list1.insertAtHead(sampleEvent2);
    	Testing.assertEquals("search on multiple in non first", 1, list1.getFirstIndex("book club"));
    	
    	list1.removeHead();
    	list1.insertAtTail(sampleEvent2);
    	Testing.assertEquals("search on multiple in first", 0, list1.getFirstIndex("book club"));

    	list1.insertAtHead(sampleEvent1);
    	Testing.assertEquals("search on duplicates", 0, list1.getFirstIndex("book club"));
    	
    	Testing.assertEquals("works for all inputs", 2, list1.getFirstIndex("chess club"));
    }

}
