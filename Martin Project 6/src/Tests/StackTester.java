package Tests;
import Utils.Stack;

/**
 * Runs a series of test on Stack ADT
 * @author Corbin Martin
 */
public class StackTester {

	/**
	 * Runs each of the tests
	 * @param args
	 */
    public static void runTests() {
        testToStringAndPush();
        testPeek();
        testPop();
        testPush();
        testIsEmpty();
        
    }
    
    private static void testIsEmpty() {
    	Testing.testSection("STACK: Testing isEmpty");
    	Stack<String> stack = new Stack<String>();
    	
    	Testing.assertEquals("Default isEmpty", true, stack.isEmpty());
    	Testing.assertEquals("Default size is 0", 0, stack.size());
    	
    	stack.push("a");
    	Testing.assertEquals("singleton is not empty", false, stack.isEmpty());
    	
    	stack.push("b");
    	Testing.assertEquals("non empty is not empty", false, stack.isEmpty());
    	
    	stack.pop();
    	stack.pop();
    	Testing.assertEquals("Empty manipulated is empty", true, stack.isEmpty());
    	
    	stack.push(null);
    	Testing.assertEquals("Null input is not empty", false, stack.isEmpty());
    	
    }
    
    private static void testPush() {
    	Testing.testSection("STACK: Testing push");
    	Stack<String> stack = new Stack<String>();
    	
    	stack.push("a");
    	Testing.assertEquals("Push to empty", "{>a}", stack.toString());
    	Testing.assertEquals("Push increases size singleton", 1, stack.size());
    	
    	stack.push("a");
    	Testing.assertEquals("Push duplicates", "{>a, a}", stack.toString());
    	Testing.assertEquals("Push increases size", 2, stack.size());
    	
    	stack.push("b");
    	Testing.assertEquals("Push multiple", "{>b, a, a}", stack.toString());
    	
    	stack.push(null);
    	Testing.assertEquals("Push null", null, stack.pop());
    	
    }
    
    private static void testPop() {
    	Testing.testSection("STACK: Testing pop");
    	Stack<String> stack = new Stack<String>();
    	
    	boolean exception = false;
    	try {
    		stack.pop();
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("pop on default is error", true, 
    			exception);
    	
    	stack.push("a");
    	Testing.assertEquals("pop on singleton", "a", stack.pop());
    	Testing.assertEquals("Pop decreases size singleton", 0, stack.size());
    	
    	stack.push("a");
    	stack.push("b");
    	stack.push("c");
    	Testing.assertEquals("pop on multiple", "c", stack.pop());
    	Testing.assertEquals("pop decreases size multiple", 2, stack.size());
    	
    	stack.pop();
    	stack.pop();
    	exception = false;
    	try {
    		stack.pop();
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("pop on empty is error", true, 
    			exception);
    }
    
    private static void testPeek() {
    	Testing.testSection("STACK: Testing peek");
    	Stack<String> stack = new Stack<String>();
    	
    	boolean exception = false;
    	try {
    		stack.peek();
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("peek on default is error", true, 
    			exception);
    	
    	stack.push("test");
    	Testing.assertEquals("Peek on singleton", "test", stack.peek());
    	
    	stack.push("a");
    	stack.push("b");
    	Testing.assertEquals("peek on multiple", "b", stack.peek());
    	
    	stack.pop();
    	Testing.assertEquals("peek after manipulation", "a", stack.peek());
    	
    	stack.pop();
    	stack.pop();
    	exception = false;
    	try {
    		stack.peek();
    	} catch (IndexOutOfBoundsException e) {
    		exception = true;
    	}
    	Testing.assertEquals("peek on empty is error", true, 
    			exception);
    }

    private static void testToStringAndPush() {
      Testing.testSection("STACK: Testing toString and push");
      
      Stack<String> stack = new Stack<String>();
      Testing.assertEquals("An empty stack. (> indicates the top of the stack)", "{>}", stack.toString());
      
      stack.push("A");
      Testing.assertEquals("A stack with one item", "{>A}", stack.toString());
      
      stack.push("B");
      stack.push("C");
      Testing.assertEquals("A stack with several items", "{>C, B, A}", stack.toString());
    }
    
}