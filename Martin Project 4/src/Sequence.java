/**
 *I affirm that I have carried out the attached academic endeavors with full academic honesty,
 * in accordance with the Union College Honor Code and the course syllabus.
 * Creates and manages a sequence object
 * @author Corbin Martin
 */
public class Sequence
{
	private final int DEFAULT_CAPACITY = 10;
	private String[] data;
	private int size;
	private int current;

    /**
     * Creates a new sequence with initial capacity 10.
     */
    public Sequence() {
    	data = new String[DEFAULT_CAPACITY];   
    	size = 0;
    	current = -1;
    }
    

    /**
     * Creates a new sequence.
     * 
     * @param initialCapacity the initial capacity of the sequence.
     */
    public Sequence(int initialCapacity){
    	data = new String[initialCapacity];
    	size = 0;
    	current = -1;
    }
    

    /**
     * Adds a string to the sequence in the location before the
     * current element. If the sequence has no current element, the
     * string is added to the beginning of the sequence.
     *
     * The added element becomes the current element.
     *
     * If the sequences's capacity has been reached, the sequence will
     * expand to twice its current capacity plus 1.
     *
     * @param value the string to add.
     */
    public void addBefore(String value)
    {
    	int pushIndex = current;
    	
    	if (!isCurrent()) {
    		pushIndex = 0;
    		current = 0;
    	}
    	
    	checkAndPushCapacity();
    	pushItemsForward(pushIndex);
    	data[pushIndex] = value;
    }
    
    /**
     * Adds a string to the sequence in the location after the current
     * element. If the sequence has no current element, the string is
     * added to the end of the sequence.
     *
     * The added element becomes the current element.
     *
     * If the sequences's capacity has been reached, the sequence will
     * expand to twice its current capacity plus 1.
     *
     * @param value the string to add.
     */
    public void addAfter(String value)
    {
    	int pushIndex = current + 1;
    	
    	if (!isCurrent()) {
    		pushIndex = size();
    		current = size();
    	} else {
    		current++;
    	}
    	
    	checkAndPushCapacity();
    	pushItemsForward(pushIndex);
    	data[pushIndex] = value;
    }
    
    /**
     * Only useful for a single item add. Using for more than 1 item
     * can result in a unsuitable capacity. 
     */
    private void checkAndPushCapacity()
    {
    	if (size() + 1 > data.length) {
    		setCapacity(2 * size() + 1);
    	}
    }
    
    
    /**
     * Push all items at and past pushIndex forward by 1. Assumes safe
     * capacity is available, if not data will be lost
     * @param pushIndex index in data to push from
     */
    private void pushItemsForward(int pushIndex) 
    {
    	String currentString = data[pushIndex];
    	String nextString;
    	
    	for (int i = pushIndex; i < size() && i + 1 < data.length; i++) {
    		nextString = data[i+1];
    		data[i+1] = currentString;
    		currentString = nextString;
    	}
    	size++;
    }
    
    private void pushItemsBackwards(int pushIndex)
    {
    	String currentString = data[pushIndex];
    	String nextString;
    	
    	for (int i = size() - 1; i > pushIndex - 1 && i > 0; i--) {
    		nextString = data[i-1];
    		data[i-1] = currentString;
    		currentString = nextString;
    	}
    	size--;
    }

    
    /**
     * @return true if and only if the sequence has a current element.
     */
    public boolean isCurrent()
    {
    	if (current < size() && current >= 0)
    		return true;
    	
    	return false;
    }
    
    
    /**
     * @return the capacity of the sequence.
     */
    public int getCapacity()
    {
    	return data.length;
    }

    
    /**
     * @return the element at the current location in the sequence, or
     * null if there is no current element.
     */
    public String getCurrent()
    {
    	if (isCurrent())
    		return data[current];
    	
    	return null;
    }
    
    
    /**
     * Increase the sequence's capacity to be
     * at least minCapacity.  Does nothing
     * if current capacity is already >= minCapacity.
     *
     * @param minCapacity the minimum capacity that the sequence
     * should now have.
     */
    public void ensureCapacity(int minCapacity)
    {
    	if (getCapacity() < minCapacity) {
    		setCapacity(minCapacity);
    	}
    }
    
    /**
     * Sets the sequence's capacity to exactly
     * the given capacity. Will copy all data up
     * to that point in sequential order.
     * 
     * @param capacity The exact capacity that the sequence 
     * should now have.
     */
    private void setCapacity(int capacity) {
    	String[] tempArray = new String[capacity];
		for (int i = 0; i < data.length && i < capacity; i++)
			tempArray[i] = data[i];
		
		data = tempArray;
    }

    
    /**
     * Places the contents of another sequence at the end of this sequence.
     *
     * If adding all elements of the other sequence would exceed the
     * capacity of this sequence, the capacity is changed to make room for
     * all of the elements to be added.
     * 
     * Postcondition: NO SIDE EFFECTS!  the other sequence should be left
     * unchanged.  The current element of both sequences should remain
     * where they are. (When this method ends, the current element
     * should refer to the same element that it did at the time this method
     * started.)
     *
     * @param another the sequence whose contents should be added.
     */
    public void addAll(Sequence another)
    {
    	int cachedPosition = current;
    	Sequence cloneOther = another.clone();
    	current = size() - 1;
    	
    	ensureCapacity(size() + cloneOther.size());
    	cloneOther.start();
    	while(cloneOther.isCurrent()) {
    		this.addAfter(cloneOther.getCurrent());
    		cloneOther.advance();
    	}
    	
    	current = cachedPosition;
    }

    
    /**
     * Move forward in the sequence so that the current element is now
     * the next element in the sequence.
     *
     * If the current element was already the end of the sequence,
     * then advancing causes there to be no current element.
     *
     * If there is no current element to begin with, do nothing.
     */
    public void advance()
    {
    	current++;
    	if (!isCurrent())
    		current = -1;
    }

    
    /**
     * Make a copy of this sequence.  Subsequence changes to the copy
     * do not affect the current sequence, and vice versa.
     * 
     * Postcondition: NO SIDE EFFECTS!  This sequence's current
     * element should remain unchanged.  The clone's current
     * element will correspond to the same place as in the original.
     *
     * @return the copy of this sequence.
     */
    public Sequence clone()
    {
    	Sequence clone = new Sequence(data.length);
    	
    	for (int i = 0; i < size(); i++) {
    		clone.addAfter(data[i]);
    	}
    	
    	
    	clone.start();
    	for (int i = 0; i < current; i++)
    		clone.advance();
    	
    	if (current == -1)
    		clone.advance();
    	
    	return clone;
    }
   
    
    /**
     * Remove the current element from this sequence.  The following
     * element, if there was one, becomes the current element.  If
     * there was no following element (current was at the end of the
     * sequence), the sequence now has no current element.
     *
     * If there is no current element, does nothing.
     */
    public void removeCurrent()
    {
    	if (isCurrent())
    		pushItemsBackwards(current+1);
    	
    	if (!isCurrent())
    		current = -1;
    }

    
    /**
     * @return the number of elements stored in the sequence.
     */
    public int size()
    {
    	return size;
    }

    
    /**
     * Sets the current element to the start of the sequence.  If the
     * sequence is empty, the sequence has no current element.
     */
    public void start()
    {
    	if (size() > 0)
    		current = 0;
    	else
    		current = -1;
    }

    
    /**
     * Reduce the current capacity to its actual size, so that it has
     * capacity to store only the elements currently stored.
     */
    public void trimToSize()
    {
    	setCapacity(size());
    }
    
    
    /**
     * Produce a string representation of this sequence.  The current
     * location is indicated by a >.  For example, a sequence with "A"
     * followed by "B", where "B" is the current element, and the
     * capacity is 5, would print as:
     * 
     *    {A, >B} (capacity = 5)
     * 
     * The string you create should be formatted like the above example,
     * with a comma following each element, no comma following the
     * last element, and all on a single line.  An empty sequence
     * should give back "{}" followed by its capacity.
     * 
     * @return a string representation of this sequence.
     */
    public String toString() 
    {
    	String concatString = "{";
    	for (int i = 0; i < size(); i++) {
    		if (i == current)
    			concatString += ">";
    		
    		concatString += data[i];
    		
    		if (i < size()-1) 
    			concatString += ", ";
    	}
    	
    	concatString += "} (capacity = " + getCapacity() + ")";
    	return concatString;
    }
    
    /**
     * Checks whether another sequence is equal to this one.  To be
     * considered equal, the other sequence must have the same size
     * as this sequence, have the same elements, in the same
     * order, and with the same element marked
     * current.  The capacity can differ.
     * 
     * Postcondition: NO SIDE EFFECTS!  this sequence and the
     * other sequence should remain unchanged, including the
     * current element.
     * 
     * @param other the other Sequence with which to compare
     * @return true iff the other sequence is equal to this one.
     */
    public boolean equals(Sequence other) 
    {
    	if (other.size != this.size())
    		return false;
    	
    	if (other.isCurrent() != this.isCurrent())
    		return false;
    	
    	Sequence cloneCompare = other.clone();
    	Sequence cloneSelf = clone();
    	
    	
    	//Check from current position to end and make sure all matches
    	int currentTillEnd = 0;
    	while (cloneCompare.isCurrent()) {
    		if (!(cloneCompare.getCurrent().equals(cloneSelf.getCurrent())))
    			return false;
    	
    		cloneCompare.advance();
    		cloneSelf.advance();
    		currentTillEnd++;
    	}
    	
    	//If Self current has an item then they are not equal
    	if (cloneSelf.isCurrent())
    		return false;
    	
    	cloneCompare.start();
    	cloneSelf.start();
    	
    	//Check from beginning to previous position for matching
    	for (int i = 0; i < cloneCompare.size - currentTillEnd; i++) {
    		if (!(cloneCompare.getCurrent().equals(cloneSelf.getCurrent())))
    			return false;
    		
    		cloneCompare.advance();
    		cloneSelf.advance();
    	}
    	
    	return true;
    }
    
    
    /**
     * 
     * @return true if Sequence empty, else false
     */
    public boolean isEmpty()
    {
    	return (size() == 0);
    }
    
    
    /**
     *  empty the sequence.  There should be no current element.
     */
    public void clear()
    {	
    	size = 0;
    	current = -1;
    	
    }

}


/**
 *  Runs a series of tests on Sequence data structure
 * @author Corbin Martin
 */
public class SequenceTests {
    
    public static void main(String[] args)
    {
    	Testing.setVerbose(true); // use false for less testing output
		Testing.startTests();

    	testCreate();
    	testToString();
    	testAddAfter();
    	testAddBefore();
    	testRemoveCurrent();
    	testAdvance();
    	testIsCurrent();
    	testIsEmpty();
    	testClear();
    	testGetCurrent();
    	testGetCapacity();
    	testEquals();
    	testClone();
    	testTrimToSize();
    	testStart();
    	testSize();
    	testAddAll();
    	testEnsureCapacity();
	
    	Testing.finishTests();
    }
    
    private static void testClear() {
    	Testing.testSection("Testing clear");
    	
    	Sequence s1 = new Sequence();
    	s1.clear();
    	Testing.assertEquals("Empty cleared is still empty", true, s1.isEmpty());
    	
    	s1.addAfter("a");
    	s1.clear();
    	Testing.assertEquals("Normal sequence (non full) is emptied", true, s1.isEmpty());
    	Testing.assertEquals("Cleared sequence has no current", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	s1.trimToSize();
    	s1.clear();
    	Testing.assertEquals("Sequence at capacity is emptied", true, s1.isEmpty());
    	
    	Sequence s2 = new Sequence(0);
    	s2.clear();
    	Testing.assertEquals("No capacity sequence is emptied", true, s2.isEmpty());
    }
    
    private static void testIsEmpty() {
    	Testing.testSection("Testing isEmpty");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("Default is empty", true, s1.isEmpty());
    	
    	s1.addAfter("a");
    	Testing.assertEquals("Default with value is not empty", false, s1.isEmpty());
    	
    	s1.addAfter("b");
    	s1.start();
    	s1.removeCurrent();
    	Testing.assertEquals("Non empty with removed is not empty", false, s1.isEmpty());
    	
    	s1.removeCurrent();
    	Testing.assertEquals("Sequeunce with all removed values is empty", true, s1.isEmpty());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("No capacity sequence is empty", true, s2.isEmpty());
    }
    
    private static void testEquals() {
    	Testing.testSection("Testing equals");
    	
    	Sequence s1 = new Sequence();
    	Sequence s2 = new Sequence();
    	Sequence s3 = new Sequence(5);
    	Testing.assertEquals("Equals with two empty", true, s1.equals(s2));
    	
    	s1.addAfter("a");
    	Testing.assertEquals("Not equals with values and no values", false, s1.equals(s2));
    	
    	s2.addAfter("a");
    	Testing.assertEquals("Equals with values in same order", true, s1.equals(s2));
    	
    	s1.advance();
    	Testing.assertEquals("Not equals with one with current one without", false, s1.equals(s2));
    	
    	s2.advance();
    	Testing.assertEquals("Equals with both no current value", true, s1.equals(s2));
    	
    	s1.start();
    	s1.addAfter("b");
    	s1.start();
    	Testing.assertEquals("Not Equals with different values", false, s1.equals(s2));
    	
    	s2.addBefore("b");
    	Testing.assertEquals("Not equals with different order", false, s1.equals(s2));
    	
	
    }
    
    private static void testToString() {
    	Testing.testSection("Testing toString");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("toString on default", "{} (capacity = 10)", s1.toString());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	Testing.assertEquals("Normal situation toString", "{a, >b} (capacity = 10)", s1.toString());
    	
    	s1.advance();
    	Testing.assertEquals("Current marked correctly past last", "{a, b} (capacity = 10)", s1.toString());
    	
    	s1.trimToSize();
    	Testing.assertEquals("Capacity correctly marked & full test", "{a, b} (capacity = 2)", s1.toString());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("Valid on no capacity", "{} (capacity = 0)", s2.toString());
    }
    
    private static void testTrimToSize() {
    	Testing.testSection("Testing trimToSize");
    	
    	Sequence s1 = new Sequence();
    	s1.trimToSize();
    	Testing.assertEquals("Trim to empty", 0, s1.getCapacity());
    	
    	s1.trimToSize();
    	Testing.assertEquals("Trim on no capacity", 0, s1.getCapacity());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	s1.trimToSize();
    	Testing.assertEquals("Trim to normal", 2, s1.getCapacity());
    	
    	
    }
    
    private static void testStart() {
    	Testing.testSection("Testing start");
    	
    	Sequence s1 = new Sequence();
    	Sequence s1Clone = s1.clone();
    	s1.start();
    	Testing.assertEquals("No changes on empty set", true, s1.equals(s1Clone));
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	s1.start();
    	Testing.assertEquals("Valid start position", "a", s1.getCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	s2.start();
    	Testing.assertEquals("No effects on starting on no capacity", false, s2.isCurrent());
    	
    	s1.trimToSize();
    	s1.start();
    	Testing.assertEquals("proper start on full capacity", "a", s1.getCurrent());
    }
    
    private static void testSize() {
    	Testing.testSection("Testing size");
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("No size on default", 0, s1.size());
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	Testing.assertEquals("valid size after add", 2, s1.size());
    	
    	s1.removeCurrent();
    	Testing.assertEquals("valid size after remove", 1, s1.size());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("No size on no capacity", 0, s2.size());
    	
    	Sequence s3 = new Sequence(2);
    	s3.addAfter("a");
    	s3.addAfter("b");
    	Testing.assertEquals("valid size on full capacity", 2, s3.size());
    }
    
    private static void testRemoveCurrent() {
    	Testing.testSection("Testing removeCurrent");
    	Sequence s1 = new Sequence();
    	
    	s1.start();
    	s1.removeCurrent();
    	Testing.assertEquals("remove on nothing", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	s1.addBefore("b");
    	s1.removeCurrent();
    	Testing.assertEquals("Remove current in normal situation",
    			"{>a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("valid size after remove", 1, s1.size());
    	
    	s1.advance();
    	s1.removeCurrent();
    	Testing.assertEquals("Remove past last item", "{a} (capacity = 10)", s1.toString());
    	
    	s1.start();
    	s1.removeCurrent();
    	Testing.assertEquals("Remove only remaining item", "{} (capacity = 10)", s1.toString());
    	
    	Sequence s2 = new Sequence(0);
    	s2.start();
    	s2.removeCurrent();
    	Testing.assertEquals("Remove on no capacity", "{} (capacity = 0)", s2.toString());
    }
    
    private static void testClone() {
    	Testing.testSection("Testing Clone");
    	Sequence s1 = new Sequence();
    	
    	s1.addAfter("a");
    	s1.addAfter("b");
    	
    	Sequence s2 = s1.clone();
    	Testing.assertEquals("Clone produces copy", true, s1.equals(s2));
    	
    	s1.addAfter("c");
    	Testing.assertEquals("Clone and parent are independent", false, s1.equals(s2));
    	
    	Sequence s3 = new Sequence(0);
    	Sequence s4 = s3.clone();
    	Testing.assertEquals("Clone of empty capacity", true, s3.equals(s4));
    	
    	Sequence s5 = new Sequence(2);
    	s5.addAfter("a");
    	s5.addAfter("b");
    	Sequence s6 = s5.clone();
    	Testing.assertEquals("Clone of full capacity", true, s5.equals(s6));
    	
    }
    
    private static void testAdvance() {
    	Testing.testSection("Testing advance");
    	
    	Sequence s1 = new Sequence();
    	
    	s1.advance();
    	Testing.assertEquals("Advance on empty still empty", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	s1.addBefore("b");
    	s1.advance();
    	Testing.assertEquals("Advance on normal set", "a", s1.getCurrent());
    	
    	s1.advance();
    	Testing.assertEquals("Advance past last is empty", false, s1.isCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	s2.advance();
    	Testing.assertEquals("Advance on no capacity", false, s2.isCurrent());
    	
    	Sequence s3 = new Sequence(1);
    	s3.addAfter("a");
    	s3.advance();
    	Testing.assertEquals("Advance at end of capacity", false, s3.isCurrent());
    }
    
    private static void testAddAll() {
    	Testing.testSection("Testing addAll");
    	Sequence s1 = new Sequence();
    	Sequence s2 = new Sequence();
    	
    	s2.addAfter("a");
    	s2.addAfter("b");
    	s2.addAfter("c");
    	s2.start();
    	s2.advance();
    	
    	Sequence s2Clone = s2.clone();
    	s1.addAll(s2);
    	Testing.assertEquals("Add to empty", "{a, b, c} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Current is still none", false, s1.isCurrent());
    	Testing.assertEquals("Added sequence is unchanged", true, s2Clone.equals(s2));
    	Testing.assertEquals("valid size after add to empty", 3, s1.size());
    	
    	Sequence s3 = new Sequence(2);
    	s3.addAfter("d");
    	s3.addAfter("e");
    	s3.addAll(s2);
    	Testing.assertEquals("Proper add to full capacity", "{d, >e, a, b, c} (capacity = 5)",
    			s3.toString());
    	Testing.assertEquals("valid size after add to full capacity", 5, s3.size());
    	
    	Sequence s4 = new Sequence(0);
    	s3.addAll(s4);
    	Testing.assertEquals("Add null sequence", "{d, >e, a, b, c} (capacity = 5)",
    			s3.toString());
    	Testing.assertEquals("valid size after add null sequence", 5, s3.size());
    	
    	s4.addAll(s3);
    	Testing.assertEquals("Add to null sequence", "{d, e, a, b, c} (capacity = 5)",
    			s4.toString());
    	Testing.assertEquals("valid size after add to null sequence", 5, s4.size());
    	
    	
    }
    
    private static void testEnsureCapacity() {
    	Testing.testSection("Testing ensureCapacity");
    	
    	Sequence s1 = new Sequence();
    	s1.ensureCapacity(5);
    	Testing.assertEquals("ensure to less than current is still current",
    			10, s1.getCapacity());
    	
    	s1.ensureCapacity(10);
    	Testing.assertEquals("Ensure to current keeps current", 10, s1.getCapacity());
    	
    	s1.ensureCapacity(15);
    	Testing.assertEquals("ensure to more than current", 15, s1.getCapacity());
    	
    	Sequence s2 = new Sequence(0);
    	s2.ensureCapacity(-1);
    	Testing.assertEquals("Doesnt crash on negative ensure", 0, s2.getCapacity());
    	
    	s2.ensureCapacity(5);
    	Testing.assertEquals("ensure works on empty capacity", 5, s2.getCapacity());
    }
    
    private static void testGetCurrent() {
    	Testing.testSection("Testing getCurrent");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("Default constructor with no items", null, s1.getCurrent());
    	
    	s1.addAfter("a");
    	Testing.assertEquals("Single Item", "a", s1.getCurrent());
    	
    	s1.addAfter("b");
    	Testing.assertEquals("Secondary item", "b", s1.getCurrent());
    	
    	s1.advance();
    	Testing.assertEquals("At end of sequence", null, s1.getCurrent());
    	
    	s1.start();
    	Testing.assertEquals("Back to first item", "a", s1.getCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("No capacity sequence", null, s2.getCurrent());
    }
    
    private static void testGetCapacity() {
    	Testing.testSection("Testing getCapacity");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("Default constructor capacity", 10, s1.getCapacity());
    	
    	Sequence s2 = new Sequence(5);
    	Testing.assertEquals("Non default constructor capacity", 5, s2.getCapacity());
    	
    	Sequence s3 = new Sequence(0);
    	Testing.assertEquals("No capacity constructor capacity", 0, s3.getCapacity());
    }
    
    private static void testIsCurrent() {
    	Testing.testSection("Testing isCurrent");
    	
    	Sequence s1 = new Sequence();
    	Testing.assertEquals("isCurrent on unused sequence", false, s1.isCurrent());
    	
    	s1.addAfter("a");
    	Testing.assertEquals("isCurrent on current item", true, s1.isCurrent());
    	
    	s1.advance();
    	Testing.assertEquals("isCurrent on advance past last item", false, s1.isCurrent());
    	
    	Sequence s2 = new Sequence(0);
    	Testing.assertEquals("isCurrent on no capacity", false, s2.isCurrent());
    }
    
    private static void testAddAfter() {
    	Testing.testSection("Testing addAfter");
    	
    	Sequence s1 = new Sequence();
    	s1.addAfter("a");
    	Testing.assertEquals("Add single item to empty", "{>a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after single add", 1, s1.size());
    	
    	s1.addAfter("b");
    	Testing.assertEquals("Add to sequence already existing", "{a, >b} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after add", 2, s1.size());
    	
    	Sequence s2 = new Sequence(0);
    	s2.addAfter("a");
    	Testing.assertEquals("Add to no capacity", "{>a} (capacity = 1)", s2.toString());
    	
    	s2.addAfter("b");
    	Testing.assertEquals("Add to filled capacity", "{a, >b} (capacity = 3)", s2.toString());
    	
    	s2.advance();
    	s2.addAfter("c");
    	Testing.assertEquals("Add when past end", "{a, b, >c} (capacity = 3)", s2.toString());
    }
    
    private static void testAddBefore() {
    	Testing.testSection("Testing addBefore");
    	
    	Sequence s1 = new Sequence();
    	s1.addBefore("a");
    	Testing.assertEquals("Add single item to empty", "{>a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after single add", 1, s1.size());
    	
    	s1.addBefore("b");
    	Testing.assertEquals("Add to sequence already existing", "{>b, a} (capacity = 10)", s1.toString());
    	Testing.assertEquals("Correct size after add", 2, s1.size());
    	
    	s1.advance();
    	s1.advance();
    	s1.addBefore("c");
    	Testing.assertEquals("Add with no current sets at beginning", "{>c, b, a} (capacity = 10)", s1.toString());
    	
    	Sequence s2 = new Sequence(0);
    	s2.addBefore("a");
    	Testing.assertEquals("Add to no capacity", "{>a} (capacity = 1)", s2.toString());
    	
    	s2.addBefore("b");
    	Testing.assertEquals("Add to filled capacity", "{>b, a} (capacity = 3)", s2.toString());
    }
    
	private static void testCreate()
	{
		Testing.testSection("Creation tests and toString of empty sequence");
		
		Sequence s1 = new Sequence();
		Testing.assertEquals("Default constructor", "{} (capacity = 10)", s1.toString());
		Testing.assertEquals("Default constructor, initial size", 0, s1.size());
		
		Sequence s2 = new Sequence(20);
		Testing.assertEquals("Non-default constructor", "{} (capacity = 20)", s2.toString());
		Testing.assertEquals("Non-default constructor, initial size", 0, s2.size());
	}
}
